<?php
require_once './_connect.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$id = escapeString($conn,strtoupper($_POST['emp_id']));
$mobile_no = escapeString($conn,strtoupper($_POST['mobile_no']));

if(strlen($mobile_no)!=10)
{
	echo "<script>
		alert('Invalid mobile number.');
		$('#emp_mob_update_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$update_terminated_emp_mobile="NO";

$chk_mobile = Qry($conn,"SELECT id,name,status FROM emp_attendance WHERE mobile_no='$mobile_no'");
if(!$chk_mobile){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_mobile)>0){
	
	$row_name = fetchArray($chk_mobile);
	
	if($row_name['status']=="-1")
	{
		$update_terminated_emp_mobile="YES";
		$terminated_emp_id = $row_name['id'];
		
		$chk_mobile2 = Qry($conn,"SELECT id FROM emp_attendance WHERE mobile_no='$mobile_no' AND id!='$row_name[id]'");
		
		if(!$chk_mobile2){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}

		if(numRows($chk_mobile2)>0)
		{
			$row_name2 = fetchArray($chk_mobile2);
			
			echo "<script>
				alert('Duplicate Mobile number. Mobile number already registered with $row_name2[name].');
				$('#emp_mob_update_btn').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}
	}
	else
	{
		echo "<script>
			alert('Duplicate Mobile number. Mobile number already registered with $row_name[name].');
			$('#emp_mob_update_btn').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}

$chk_emp = Qry($conn,"SELECT name,code,branch,mobile_no,status FROM emp_attendance WHERE id='$id'");
if(!$chk_emp){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_emp)==0){
	echo "<script>
		alert('Employee not found.');
		$('#emp_mob_update_btn').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row = fetchArray($chk_emp);

// if($row['status']!='3')
// {
	// echo "<script>
		// alert('Employee is not active.');
		// $('#emp_mob_update_btn').attr('disabled',false);
		// $('#loadicon').hide();
	// </script>";
	// exit();
// }

$emp_name = $row['name']."($row[branch])";
$emp_code = $row['code'];

if($row['mobile_no']=='')
{
	$desc = "$mobile_no";
}
else
{
	if($row['mobile_no']==$mobile_no)
	{
		echo "<script>
			alert('Nothing to update.');
			$('#emp_mob_update_btn').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$desc = "$row[mobile_no] to $mobile_no";
}

StartCommit($conn);
$flag = true;

if($update_terminated_emp_mobile=='YES')
{
	$update1 = Qry($conn,"UPDATE emp_attendance SET mobile_no='' WHERE id='$terminated_emp_id'");

	if(!$update1){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$update = Qry($conn,"UPDATE emp_attendance SET mobile_no='$mobile_no' WHERE id='$id'");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,timestamp) VALUES ('$emp_code','$emp_name',
'MOBILE_UPDATE','$desc','ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		$('#FormMobileUpdate')[0].reset()
		$('#emp_mob_update_btn').attr('disabled',false);
		alert('Updated successfully !');
		$('#mob_update_modal_close').click();
		$('#fetch_record_button').click();
		$('#loadicon').hide();
	</script>";
	exit();	
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./employee_management.php");
	exit();
}	
?>