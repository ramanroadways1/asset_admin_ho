<?php
require_once './_connect.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$id = escapeString($conn,strtoupper($_POST['id']));
$branch = escapeString($conn,strtoupper($_POST['branch']));
$branch_to = escapeString($conn,strtoupper($_POST['branch_to']));
$code = escapeString($conn,strtoupper($_POST['code']));
$emp_name = escapeString($conn,strtoupper($_POST['emp_name']));

if($id==""){
	echo "<script>
		alert('Employee id not found !');
		window.location.href='./employee_management.php';
	</script>";
	exit();
}

$GetStatus = Qry($conn,"SELECT status,mobile_no FROM emp_attendance WHERE id='$id'");
if(!$GetStatus){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error !');
		window.location.href='./employee_management.php';
	</script>";
	exit();
}

if(numRows($GetStatus)==0)
{
	echo "<script>
		alert('Employee not found !');
		window.location.href='./employee_management.php';
	</script>";
	exit();
}

$row_ChkStatus = fetchArray($GetStatus);

$emp_mobile_no = $row_ChkStatus['mobile_no'];

if($row_ChkStatus['status']!=3)
{
	echo "<script>
		alert('Employee is in-active !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_manager = Qry($conn,"SELECT id FROM manager WHERE emp_code='$code'");
if(!$chk_manager){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error !');
		window.location.href='./employee_management.php';
	</script>";
	exit();
}

if(numRows($chk_manager)>0)
{
	echo "<script>
		alert('$emp_name is manager of branch: $branch !');
		$('#loadicon').hide();
	</script>";
	exit();
}


StartCommit($conn);
$flag = true;

$new_pass = GenPassword(8);

$Transfer_Update = Qry($conn,"INSERT INTO emp_transfer(code,oldbranch,newbranch,user_code,approval_from,approval_from_timestamp,
approval_to,approval_to_timestamp,dated,timestamp) VALUES ('$code','$branch','$branch_to','ADMIN','1','$timestamp','1','$timestamp',
'$date','$timestamp')");

if(!$Transfer_Update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_emp = Qry($conn,"UPDATE emp_attendance SET password='".md5($new_pass)."',last_pass='$timestamp',branch='$branch_to',status='3',
branchtransfer='' WHERE id='$id'");

if(!$update_emp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$chk_Today_attd = Qry($conn,"SELECT id FROM emp_attd_check WHERE date='$date' AND branch='$branch_to' AND (p+a+hd)=0");
if(!$chk_Today_attd){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_Today_attd)>0)
{
	$row_attd = fetchArray($chk_Today_attd);
	
	$update_today_data = Qry($conn,"UPDATE emp_attd_check SET total=total+1 WHERE id='$row_attd[id]'");
	if(!$update_today_data){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$chk_Today_attd_from_branch = Qry($conn,"SELECT id FROM emp_attd_check WHERE date='$date' AND branch='$branch' AND (p+a+hd)=0");
if(!$chk_Today_attd_from_branch){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_Today_attd_from_branch)>0)
{
	$row_attd_from_branch = fetchArray($chk_Today_attd_from_branch);
	
	$update_today_data_from_branch = Qry($conn,"UPDATE emp_attd_check SET total=total-1 WHERE id='$row_attd_from_branch[id]'");
	if(!$update_today_data_from_branch){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,timestamp) VALUES ('$code','Employee',
'Employee_Transfer','$branch to $branch_to','ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	// MsgSendNewPasswordTransfer($emp_mobile_no,$new_pass,$emp_name,$branch_to,$code);
	
	$msg_template="Hello, $emp_name($code).\nYou are Transferred to $branch_to Branch.\nYour new password is: $new_pass.\nRamanRoadways.";
	SendWAMsg($conn,$emp_mobile_no,$msg_template);
	
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Employee : $emp_name. Successfully Transferred to $branch_to.');
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./employee_management.php");
	exit();
}
?>