<?php
require_once './_connect.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

include ("./_header.php");
?>
<div class="container-fluid" style="color:#000">

<div class="row">
	<div class="from-group col-md-5">
		<div class="row">
			<div class="col-md-8">
				<label>Branch <font color="red">*</font></label>
				<select name="branch" id="branch_nameFind" class="form-control" required>
				<option value="">--select branch--</option>
					<?php 
$FetchBranch = Qry($conn,"SELECT username FROM user WHERE role='2'");
if(!$FetchBranch){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($FetchBranch)>0)
{
	while($rowBranch = fetchArray($FetchBranch))
	{
		echo "<option value='$rowBranch[username]'>$rowBranch[username]</option>";
	}
}
					?>
				</select>
			</div>	
			<div class="col-md-4">
				<label>&nbsp;</label>
				<br />
				<button type="button" id="fetch_record_button" onclick="FetchRecord()" class="btn btn-primary">
					<span style="font-size:18px;" class="glyphicon glyphicon-share-alt"></span>
				</button>
			</div>
		</div>	
	</div>
	<div class="from-group col-md-12" id="result_div"></div>
</div>

</div>
</body>

<script type="text/javascript">
function FetchRecord(){
	var branch = $('#branch_nameFind').val();
	
	if(branch=='')
	{
		alert('Select branch first !');
	}
	else
	{
		$("#loadicon").show();
		jQuery.ajax({
		url: "./load_employee.php",
		data: 'branch=' + branch,
		type: "POST",
		success: function(data){
			$("#result_div").html(data);
		},
		error: function() {}
		});
	}
}
</script>

<script>
function Transfer(id,branch)
{
	var emp_name = $('#emp_name'+id).val();
	
	var branch_to = $('#branch_name'+id).val();
		
	if(branch_to=='')
	{
		alert('Select Transfer to branch first !');
	}
	else
	{	
		if(confirm("are you sure to transfer "+emp_name+" to "+branch_to+" ?")==true)
		{
			$('#transfer_button'+id).attr('disabled',true);
			var code = $('#emp_code'+id).val();
				
					$("#loadicon").show();
					jQuery.ajax({
					url: "./save_emp_transfer.php",
					data: 'id=' + id + '&branch=' + branch + '&branch_to=' + branch_to + '&code=' + code + '&emp_name=' + emp_name,
					type: "POST",
					success: function(data){
						$("#func_result").html(data);
					},
					error: function() {}
			});
		}
	}
}

function Terminate(id,branch)
{
	var code = $('#emp_code'+id).val();
	var emp_name = $('#emp_name'+id).val();
	
	if(confirm("are you sure to terminate "+emp_name+" ?")==true)
	{
		$('#terminate_button'+id).attr('disabled',true);
		$("#loadicon").show();
		jQuery.ajax({
		url: "./save_emp_terminate.php",
		data: 'id=' + id + '&branch=' + branch + '&code=' + code + '&emp_name=' + emp_name,
		type: "POST",
		success: function(data){
			$("#func_result").html(data);
		},
		error: function() {}
		});
	}
}

function ResetPassword(emp_id)
{
	$('#ResetPasswordBtn'+emp_id).attr('disabled',true);
	$("#loadicon").show();
	jQuery.ajax({
		url: "./reset_password_branch_employee.php",
		data: 'emp_id=' + emp_id,
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
	error: function() {}
	});
}

function SetEmpName(id)
{
	$('#emp_name_mob_update').val($('#emp_name'+id).val());
	$('#emp_mobile_mob_update').val($('#emp_mobile_no'+id).val());
	$('#emp_id_mob_update').val(id);
}

function ActiveEmployee(id)
{
	var emp_name = $('#emp_name'+id).val();
	var branch = $('#branch_name_'+id).val();
	
	if(confirm("are you sure to active "+emp_name+" ?")==true)
	{
		var code = $('#emp_code'+id).val();
		$('#active_btn'+id).attr('disabled',true);
	
		$("#loadicon").show();
			jQuery.ajax({
			url: "./save_emp_active_again.php",
			data: 'id=' + id + '&branch=' + branch + '&code=' + code + '&emp_name=' + emp_name,
			type: "POST",
			success: function(data){
				$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}

function ActiveLogin(id,branch)
{
	if(confirm("are you sure ??")==true)
	{
		var elem = $('#login_active'+id).val();
		$('#login_active_button'+id).attr('disabled',true);
	
		$("#loadicon").show();
			jQuery.ajax({
			url: "./save_emp_active_login.php",
			data: 'id=' + id + '&elem=' + elem,
			type: "POST",
			success: function(data){
				$("#func_result").html(data);
			},
			error: function() {}
		});
	}
}
</script>

<div id="func_result"></div>

<form id="FormMobileUpdate" action="#" method="POST">
<div id="Emp_Mobile_Modal" class="modal fade" style="background:#eee" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header bg-primary">
		Update Mobile :
      </div>
      <div class="modal-body">
        <div class="row">
		
			<div class="form-group col-md-12">
				<label>Employee Name <font color="red"><sup>*</sup></font></label>
				<input type="text" id="emp_name_mob_update" readonly class="form-control" required="required">
			</div>
			
			<div class="form-group col-md-12">
				<label>Mobile Number <font color="red"><sup>*</sup></font></label>
				<input type="number" id="emp_mobile_mob_update" name="mobile_no" class="form-control" required="required">
			</div>
			
			<input type="hidden" name="emp_id" id="emp_id_mob_update">
			
		</div>
      </div>
	  <div class="modal-footer">
        <button type="submit" id="emp_mob_update_btn" class="btn btn-primary">Update</button>
        <button type="button" id="mob_update_modal_close" class="btn btn-default" data-dismiss="modal">Close</button>
	  </div>
    </div>

  </div>
</div>
</form>
							
<script type="text/javascript">
$(document).ready(function (e) {
	$("#FormMobileUpdate").on('submit',(function(e) {
	e.preventDefault();
	$("#loadicon").show();
	$("#emp_mob_update_btn").attr("disabled", true);
	$.ajax({
        	url: "./save_employee_mobile.php",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {
				$("#func_result").html(data);
			},
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});
</script>