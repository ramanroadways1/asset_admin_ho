<?php
require_once './_connect.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

include ("./_header.php");
?>
<div class="container-fluid" style="color:#000">

<div class="row">
	<div class="from-group col-md-12" id="result_div"></div>
</div>

</div>
</body>

<script type="text/javascript">
function loadData(){
	$('#loadicon').show();
    $.ajax({
    url: "load_branch_managers.php",
    cache: false,
    success: function(data){
       $("#result_div").html(data);
	} 
  });
}

loadData();
</script>

<script>
function ResetPassword(emp_id,branch,code,mobile)
{
	$('#ResetPasswordBtn'+branch).attr('disabled',true);
	$("#loadicon").show();
	jQuery.ajax({
		url: "./reset_password_branch_manager.php",
		data: 'branch=' + branch + '&code=' + code + '&mobile=' + mobile + '&emp_id=' + emp_id,
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
	error: function() {}
	});
}

function ChangeManager(branch,emp_old,mobile_no)
{
	var code = $('#emp_update_code'+branch).val();
	if(code=='')
	{
		alert('Select employee first !');
	}
	else
	{
		$('#change_button'+branch).attr('disabled',true);
		$('#emp_update_code'+branch).attr('disabled',true);
		$("#loadicon").show();
		jQuery.ajax({
			url: "./save_change_manager.php",
			data: 'branch=' + branch + '&code=' + code + '&emp_old=' + emp_old,
			type: "POST",
			success: function(data) {
				$("#func_result").html(data);
			},
		error: function() {}
		});
	}	
}
</script>

<div id="func_result"></div>