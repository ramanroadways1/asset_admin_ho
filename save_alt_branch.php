<?php
require_once './_connect.php';

$branch = escapeString($conn,strtoupper($_POST['branch']));
$id = escapeString($conn,($_POST['id']));
$timestamp = date("Y-m-d H:i:s");

StartCommit($conn);
$flag = true;

$updateAltBranch = Qry($conn,"UPDATE emp_attendance SET alternate_branch=IF(alternate_branch='','$branch',CONCAT(alternate_branch,',','$branch')) WHERE id='$id'");

if(!$updateAltBranch){
	$flag = false;
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,timestamp) VALUES ('$id',
'Alt_Branch_Add','Branch_Add','$branch added.','ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		// alert('Branch Added Successfully !');
		FetchRecord(); 
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>alert('Error !');$('#loadicon').hide();</script>";
	exit();
}
?>