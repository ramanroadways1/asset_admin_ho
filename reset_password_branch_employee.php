<?php
require_once './_connect.php';

$emp_id = escapeString($conn,strtoupper($_POST['emp_id']));
$timestamp = date("Y-m-d H:i:s");

if(empty($emp_id)){
	echo "<script>
		alert('Employee not found !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_employee = Qry($conn,"SELECT name,mobile_no,code FROM emp_attendance WHERE id='$emp_id'");
if(!$chk_employee){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_check = fetchArray($chk_employee);

$mobile = $row_check['mobile_no'];
$code = $row_check['code'];
$emp_name = $row_check['name'];

$password = GenPassword(8);
// MsgSendNewPassword("Branch",$code,$mobile,$password,$emp_name);

$msg_template="Hello, $emp_name. \nYour new password is: $password.\nRamanRoadways.";
SendWAMsg($conn,$mobile,$msg_template);

StartCommit($conn);
$flag = true;

$updatePass = Qry($conn,"UPDATE emp_attendance SET password='".md5($password)."',last_pass='$timestamp' WHERE id='$emp_id'");
if(!$updatePass){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,timestamp) VALUES ('$code','Employee',
'Password_reset','Employee password reseted by admin.','ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Password reset success !');
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>
		alert('Error !!');
		$('#loadicon').hide();
	</script>";
	exit();
}
	
?>