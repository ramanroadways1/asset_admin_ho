<?php
require_once './_connect.php';
?>
<div class="row">	

		<div class="form-group col-md-4">
			<h4 style="color:#000"><i class="fa fa-mobile" aria-hidden="true"></i> &nbsp; Added Asset Approval :</h4> 
		</div>
		
	<div class="form-group col-md-12 table-responsive">
			<table class="table table-bordered table-striped" style="font-size:12px;">
				<tr style="background:#299C9B;font-size:13px;color:#FFF">
					<th>#</th>
					<th>Branch</th>
					<th>Date</th>
					<th>Token_No</th>
					<th>Catagory</th>
					<th>Asset company</th>
					<th>Asset model</th>
					<th>Invoice date</th>
					<th>General/specific</th>
					<th>Asset holder</th>
					<th>Added by</th>
					<th>Amount</th>
					<th>GST amount</th>
					<th>Total amount</th>
					<th>Approve</th>
				</tr>	
<?php
$getAsset = Qry($conn,"SELECT a.id,a.req_code,a.asset_company,a.asset_model,a.invoice_date,a.typeof,a.holder,a.branch,a.branch_user,
a.amount,a.gst_amount,a.total_amount,date(a.timestamp) as date,c.title,u.name as added_by,u2.name as holder1
FROM asset_main AS a 
LEFT OUTER JOIN asset_category as c ON c.id=a.category
LEFT OUTER JOIN emp_attendance AS u ON u.code=a.branch_user 
LEFT OUTER JOIN emp_attendance AS u2 ON u2.code=a.holder 
WHERE a.ho_approval=0 ORDER by a.id ASC");

if(!$getAsset){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($getAsset)>0)
{
	$sn1=1;
	while($row = fetchArray($getAsset))
	{
		if($row['branch']==$row['branch_user']){
			$branch_user = $row['branch'];
		}
		else{
			$branch_user = $row['added_by'];
		}
		
		if($row['branch']==$row['holder']){
			$asset_holder = $row['branch'];
		}
		else{
			$asset_holder = $row['holder1'];
		}
		
		$req_date = convertDate("d-m-y",$row["date"]);
		$invoice_date = convertDate("d-m-y",$row["invoice_date"]);
		
		echo "<tr>
			<td>$sn1</td>
			<td>$row[branch]</td>
			<td>$req_date</td>
			<td>$row[req_code]</td>
			<td>$row[title]</td>
			<td>$row[asset_company]</td>
			<td>$row[asset_model]</td>
			<td>$invoice_date</td>
			<td>$row[typeof]</td>
			<td>$asset_holder</td>
			<td>$branch_user</td>
			<td>$row[amount]</td>
			<td>$row[gst_amount]</td>
			<td>$row[total_amount]</td>
			<td>
				<button type='button' id='ApproveAsset$row[id]' onclick='ApproveAsset($row[id])' 
				class='btn btn-sm btn-success'><span class='glyphicon glyphicon-thumbs-up'></span> Approve</a>
			</td>
		</tr>";
		
	$sn1++;	
	}
}
else
{
	echo "<tr><td colspan='15'>No records found.</td></tr>";
}
			?>			
			</table>
		</div>
		
	</div>
	
<script>
	$('#loadicon').hide();
</script>	