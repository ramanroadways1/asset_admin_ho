<?php
require_once './_connect.php';
?>
<div class="row">	

		<div class="form-group col-md-4">
			<h4 style="color:#000"><i class="fa fa-user-circle-o" aria-hidden="true"></i> &nbsp; Branch Managers :</h4> 
		</div>
		
	<div class="form-group col-md-12 table-responsive">
			<table class="table table-bordered table-striped" style="font-size:12px;">
				<tr style="background:#299C9B;font-size:13px;color:#FFF">
					<th>#</th>
					<th>Branch</th>
					<th>Emp.Code</th>
					<th>Full Name</th>
					<th>Mobile</th>
					<th>Last Password Updated</th>
					<th>Reset Password</th>
					<th>Change Manager</th>
				</tr>	
<?php
$getManagers = Qry($conn,"SELECT m.emp_code,m.branch,m.last_pass,a.name,a.mobile_no,a.id 
FROM manager AS m 
LEFT OUTER JOIN emp_attendance as a ON a.code=m.emp_code 
ORDER BY m.branch ASC");

if(!$getManagers){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($getManagers)>0)
{
	$sn1=1;
	while($row = fetchArray($getManagers))
	{
		echo "<tr>
			<td>$sn1</td>
			<td>$row[branch]</td>
			<td>$row[emp_code]</td>
			<td>$row[name]</td>
			<td>$row[mobile_no]</td>
			<td>$row[last_pass]</td>
			<td>
				<button type='button' id='ResetPasswordBtn$row[branch]' onclick=ResetPassword('$row[id]','$row[branch]','$row[emp_code]','$row[mobile_no]')
				class='btn btn-xs btn-danger'><i class='fa fa-refresh' aria-hidden='true'></i> Reset password</a>
			</td>
			<td>
			
			<div class='form-inline'>
			<select class='form-control' style='width:120px;font-size:12px;height:30px;' id='emp_update_code$row[branch]'>
				<option value='' style='text-transform:lowercase'>--select--</option>"; 
				$get_all_emp = Qry($conn,"SELECT id,code,name FROM emp_attendance WHERE code!='$row[emp_code]' AND branch='$row[branch]' 
				AND status='3' ORDER BY code ASC");

				if(!$get_all_emp){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error while processing Request","./");
					exit();
				}

				if(numRows($get_all_emp)>0)
				{
					while($row_emp = fetchArray($get_all_emp))
					{
						echo "<option value='$row_emp[code]'>$row_emp[name]</option>"; 
					}
				}
				else
				{
					echo "<option value='NO_MANAGER' style='text-transform:lowercase'>no manager</option>"; 
				}
				
			echo "
			</select>
<button type='button' id='change_button$row[branch]' onclick=ChangeManager('$row[branch]','$row[emp_code]','$row[mobile_no]')
class='btn btn-primary'><span style='font-size:px;' class='glyphicon glyphicon-share-alt'></span></button></div>
	
			</td>
		</tr>";
		
	$sn1++;	
	}
}
else
{
	echo "<tr><td colspan='10'>No records found.</td></tr>";
}
			?>			
			</table>
		</div>
		
	</div>
	
<script>
	$('#loadicon').hide();
</script>	