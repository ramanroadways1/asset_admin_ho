<?php
require_once './_connect.php';

$branch = escapeString($conn,strtoupper($_POST['branch']));
$id = escapeString($conn,($_POST['id']));
$timestamp = date("Y-m-d H:i:s");

StartCommit($conn);
$flag = true;

$get_new_record = Qry($conn,"SELECT alternate_branch FROM emp_attendance WHERE id='$id'");

if(!$get_new_record){
	$flag = false;
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$row2 = fetchArray($get_new_record);

// if(strlen($row2['alternate_branch'])<=2)
// {
	// echo "O";
	// $new_branches = "";
// }
// else
// {

$branch_str = trim(str_replace($branch,"",$row2['alternate_branch']));
$parts=explode(",",$branch_str);
$parts=array_filter($parts);
$new_branches = (implode(",",$parts));
// }

// echo $new_branches;

// echo "<script>
		// $('#loadicon').hide();
	// </script>";
	// exit();
	
$update_new = Qry($conn,"UPDATE emp_attendance SET alternate_branch='$new_branches' WHERE id='$id'");

if(!$update_new){
	$flag = false;
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,timestamp) VALUES ('$id','Alt_Branch_Remove',
'Branch_Remove','$branch removed.','ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		FetchRecord(); 
		// $('#loadicon').hide();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	echo "<script>alert('Error !');$('#loadicon').hide();</script>";
	exit();
}
?>