<?php
require_once './_connect.php';

$branch = escapeString($conn,strtoupper($_POST['branch']));
$code = escapeString($conn,strtoupper($_POST['code']));
$emp_old = escapeString($conn,strtoupper($_POST['emp_old']));
$timestamp = date("Y-m-d H:i:s");

if($code==''){
	echo "<script>
		alert('Employee code is empty !');
		$('#change_button$branch').attr('disabled',false);
		$('#emp_update_code$branch').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

if($code!='NO_MANAGER')
{
	$getMobile = Qry($conn,"SELECT name,mobile_no FROM emp_attendance WHERE code='$code'");
	if(!$getMobile){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		Redirect("Error while processing Request","./");
		exit();
	}

	$rowMobile = fetchArray($getMobile);

	$mobile = $rowMobile['mobile_no'];
	$emp_name = $rowMobile['name'];

	if($mobile=='' || (strlen($mobile)!=10)){
		echo "<script>
			alert('Employee mobile number is not valid !');
			$('#change_button$branch').attr('disabled',false);
			$('#emp_update_code$branch').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}

	$password = GenPassword(8);
	MsgChangeManager($emp_name,$mobile,$password,$branch);
}

StartCommit($conn);
$flag = true;

if($code!='NO_MANAGER')
{
	$updatePass = Qry($conn,"UPDATE manager SET emp_code='$code',pass='".md5($password)."',last_pass='$timestamp' WHERE branch='$branch'");
	if(!$updatePass){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,timestamp) VALUES ('$branch','Manager',
	'Manager_update','Manager updated $emp_old to $code.','ADMIN','$timestamp')");

	if(!$insert_log){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$updatePass = Qry($conn,"UPDATE manager SET emp_code='',pass='".md5($password)."',last_pass='$timestamp' WHERE branch='$branch'");
	if(!$updatePass){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}

	$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,timestamp) VALUES ('$branch','Manager',
	'Manager_update','Branch: $branch marked as no manager.','ADMIN','$timestamp')");

	if(!$insert_log){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}	
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Manager Update Success !');
		$('#change_button$branch').attr('disabled',true);
		$('#emp_update_code$branch').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./branch_managers.php");
	exit();
}
?>