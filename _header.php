<head>
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta name="robots" content="noindex,nofollow"/>
<title>RAMAN ROADWAYS PVT. LTD. || A RAMAN GROUP OF COMPANY.</title>
<link rel="icon" type="image/png" href="../../favicon.png" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link href="../../b5aY6EZzK52NA8F/google_font.css" rel="stylesheet">
<link rel="stylesheet" href="../../b5aY6EZzK52NA8F/font-awesome-4.7.0/css/font-awesome.min.css">
<link href="../../b5aY6EZzK52NA8F/css/styles.css" rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>  
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>  
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>  
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>  
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap.min.js"></script>  
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css" rel="stylesheet">

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  

<?php 
include("./_loadicon.php");
include("../../b5aY6EZzK52NA8F/disable_right_click.php");
	
$menu_page_name = basename($_SERVER['PHP_SELF']);
$menu_page_name2=$_SERVER['QUERY_STRING'];;
?>

<style> 
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
.form-control
{
	border:1px solid #000;
	text-transform:uppercase;
}

#active{
	background-color:green;
}

#active>#DropDownLink{
	color:#FFF;
}

.dropdown-menu>li>a{
	padding:6px;
}

#DropDownLink{
	color:#000;
}

#DropDownLink:hover{
	background-color:DodgerBlue;
	color:#FFF;
}
.ui-autocomplete { z-index:2147483647; } 

ul>li:hover{
	background-color:#333;
}
 </style> 

<style>
 .dataTables_scroll{ margin-bottom: 20px;}
 .table {margin:0px !important;}
.ui-autocomplete { z-index:2147483647; } 
</style>

<style>
::-webkit-scrollbar{
    width: 4px;
    height: 4px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 4px;
}
::-webkit-scrollbar-thumb {
    border-radius: 4px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}
</style>

</head>

<body style="background-color:#FFF;color:#000;font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);">

<nav class="navbar navbar-inverse navbar-static-top" style="margin-top:-51px;background-color:MediumSeaGreen;font-size:13px;">
  <div class="container-fluid">
   <div class="navbar-header">
       <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
       </button>
 </div>
   
<div id="navbar" class="navbar-collapse collapse">   
    <ul class="nav navbar-nav"><!--;-->
      <li id="<?php if($menu_page_name=="index.php") {echo "active";} ?>"><a style="color:#FFF" href="./">Home</a></li>
      <li id="<?php if($menu_page_name=="employee_management.php") {echo "active";} ?>"><a style="color:#FFF" href="./employee_management.php">Employee Management</a></li>
      <li id="<?php if($menu_page_name=="asset_approval.php") {echo "active";} ?>"><a style="color:#FFF" href="./asset_approval.php">Asset Approval</a></li>
      <li id="<?php if($menu_page_name=="asset_approval_after_added.php") {echo "active";} ?>"><a style="color:#FFF" href="./asset_approval_after_added.php">Asset Approval (added)</a></li>
      <li id="<?php if($menu_page_name=="vehicle_approval.php") {echo "active";} ?>"><a style="color:#FFF" href="./vehicle_approval.php">Vehicle Approval</a></li>
      <li id="<?php if($menu_page_name=="vehicle_approval_after_added.php") {echo "active";} ?>"><a style="color:#FFF" href="./vehicle_approval_after_added.php">Vehicle Approval (added)</a></li>
      <li id="<?php if($menu_page_name=="branch_managers.php") {echo "active";} ?>"><a style="color:#FFF" href="./branch_managers.php">Branch Managers</a></li>
    </ul>
	
	<ul class="nav navbar-nav navbar-right">
	   <a <?php if(isset($_SESSION['user_rkg'])) { echo "href='https://rrpl.online/rkg/'"; } else { echo "href='../'"; } ?>><button style="margin:10px;" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Go back</button></a>
       <a href="./logout.php"><button style="margin:10px;" class="btn btn-sm btn-danger"><i class="fa fa-sign-out" aria-hidden="true"></i> Log out</button></a>
    </ul>
</div>		
</div>
</nav>