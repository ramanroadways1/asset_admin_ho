<?php
require_once './_connect.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

include ("./_header.php");
?>
<div class="container-fluid" style="color:#000">

<div class="row">
	<div class="from-group col-md-12" id="result_div"></div>
</div>

</div>
</body>

<script type="text/javascript">
function load_Approvals(){
	$('#loadicon').show();
    $.ajax({
    url: "load_asset_approvals_for_added.php",
    cache: false,
    success: function(data){
       $("#result_div").html(data);
	} 
  });
}

load_Approvals();
</script>

<script>
function DeleteReq(id)
{
	if(confirm("You you really want to delete this request ?")==true)
	{
		// $('#DeleteReq'+id).attr('disabled',true);
		// $('#ApproveAsset'+id).attr('disabled',true);
		
		// $("#loadicon").show();
		// jQuery.ajax({
		// url: "./delete_asset_req.php",
		// data: 'id=' + id + '&type=' + 'ASSET',
		// type: "POST",
		// success: function(data) {
			// $("#func_result").html(data);
		// },
		// error: function() {}
		// });
	}
}

function ApproveAsset(id)
{
	if(confirm("Do you really want to approve ?")==true)
	{
		$('#loadicon').show();
		$('#ApproveAsset'+id).attr('disabled',true);
		
		$("#loadicon").show();
		jQuery.ajax({
		url: "./approve_added_asset.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
		});
	}
}
</script>

<div id="func_result"></div>
