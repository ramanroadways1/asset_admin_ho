<?php
require_once './_connect.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$id = escapeString($conn,strtoupper($_POST['id']));
$toggle = escapeString($conn,strtoupper($_POST['toggle']));

StartCommit($conn);
$flag = true;

$update_toggle = Qry($conn,"UPDATE emp_attendance SET whatsapp_enabled='$toggle' WHERE id='$id'");

if(!$update_toggle){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('OK : Done !');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./employee_management.php");
	exit();
}
?>