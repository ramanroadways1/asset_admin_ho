<?php
require_once './_connect.php';

$branch = escapeString($conn,strtoupper($_POST['branch']));
?>
<br />		
	<div class="row">	
		<div class="form-group col-md-4">
			<h5><span class="glyphicon glyphicon-user"></span> &nbsp; All Employee : <font color="red">(<?php echo $branch; ?>)</font></h5> 
		</div>
		
		<div class="form-group col-md-12 table-responsive">
			<table class="table table-bordered table-striped" style="font-size:12px;">
				<tr style="background:#299C9B;font-size:13px;color:#FFF">
					<th>#</th>
					<th>Name</th>
					<th>Code</th>
					<th>Joining Date</th>
					<th>Father</th>
					<th>Mobile</th>
					<th>PAN</th>
					<th>View</th>
					<th>Status</th>
					<th>Transfer</th>
					<th>Active/Block Login</th>
				<!--	<th>Diesel_Entry<br>(e-Diary)</th> -->
					<th>Alternate Branch</th>
					<th>Whatsapp Enabled</th>
					<th>Terminate</th>
					<th>Password</th>
				</tr>	
<?php
$view_emp = Qry($conn,"SELECT id,name,active_login,login_type,code,branch,diesel_entry,alternate_branch,join_date,father_name,mobile_no,
whatsapp_enabled,acc_pan,status,branchtransfer FROM emp_attendance WHERE branch='$branch' ORDER by code ASC");

if(!$view_emp){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($view_emp)>0)
{
	$sn=1;
	while($row = fetchArray($view_emp))
	{
		if($row['join_date']==0){
			$join_date = "NULL";
		}
		else
		{
			$join_date = convertDate("d-m-y",$row["join_date"]);
		}
		
		if($row['status']=="-1"){
			$emp_status="<font color='red'>Terminated</font>
			<br>
			<button type='button' id='active_btn$row[id]' class='btn btn-xs btn-success' onclick='ActiveEmployee($row[id])'>Active</button>";
		}
		else if($row['status']=="3"){
			$emp_status="<font color='green'>Active</font>";
		}
		else if($row['status']=="2"){
			$emp_status="<font color='blue'>Transfer_Initiated</font>";
		}
		else{
			$emp_status="<font color='red'>Others</font>";
		}
		
		if($row['diesel_entry']=="1")
		{
			$diesel_entry_chk = "<input type='checkbox' onchange='DieselEntryEd($row[id])' id='diesel_entry_$row[id]' checked='checked' value='1' />";
		}
		else
		{
			$diesel_entry_chk = "<input type='checkbox' onchange='DieselEntryEd($row[id])' id='diesel_entry_$row[id]' value='0' />";
		}
		
		$disable_alt_branch="disabled";
		
		if($row['status']=="2"){
			$disable_transfer="disabled";
			$name_btn_transfer="Transferred";
		}
		else{
			$disable_transfer="";
			$name_btn_transfer="Transfer";
		}
		
		if($row['status']=="3"){
			$disable_tremiate="";
			$disable_alt_branch="";
			$name_btn_tremiate="Terminate";
		}
		else{
			$disable_tremiate="disabled";
			$name_btn_tremiate="In-Active";
		}
		
	echo "<input type='hidden' value='$row[name]' id='emp_name$row[id]'>";
	echo "<input type='hidden' value='$row[code]' id='emp_code$row[id]'>";	
	echo "<input type='hidden' value='$row[mobile_no]' id='emp_mobile_no$row[id]'>";	
	echo "<input type='hidden' value='$row[branch]' id='branch_name_$row[id]'>";	
	
		echo "<tr>
			<td>$sn</td>
			<td>$row[name]</td>
			<td>$row[code]</td>
			<td>$join_date</td>
			<td>$row[father_name]</td>
			<td>";
			if($row['mobile_no']!='')
			{
				echo "$row[mobile_no] &nbsp; <a onclick=SetEmpName('$row[id]') style='color:#FFF' data-toggle='modal' data-target='#Emp_Mobile_Modal' href='#' class='btn btn-xs btn-warning'>
				<span class='glyphicon glyphicon-pencil' style='padding:2px;font-size:10px'></span></a>";
			}
			else
			{
				echo "<a onclick=SetEmpName('$row[id]') style='color:#FFF' data-toggle='modal' data-target='#Emp_Mobile_Modal' href='#' class='btn btn-xs btn-primary'>
				<span class='glyphicon glyphicon-add'></span> Add mobile</a>";
			}
			echo "</td>
			<td>$row[acc_pan]</td>
			<td><a style='color:#000' target='_blank' href='employee_view_full.php?id=$row[id]' class='btn btn-sm btn-default'>
			<span class='glyphicon glyphicon-list-alt'></span></a></td>
			<td id='emp_status_td$row[id]'>$emp_status</td>
			<td>";
			if($row['status']=="2")
			{
				echo "Transferred to <font color='red'>$row[branchtransfer]</font>";
			}
			else
			{
			echo "
			<div class='form-inline'>
			<select class='form-control' style='width:120px;font-size:12px;height:30px;' id='branch_name$row[id]'>
				<option value=''>--branch--</option>"; 
				$get_all_branch = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('$branch','HEAD','DUMMY') 
				ORDER by username ASC");

				if(!$get_all_branch){
					errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
					Redirect("Error while processing Request","./");
					exit();
				}

				if(numRows($get_all_branch)>0)
				{
					while($row_branch = fetchArray($get_all_branch))
					{
						echo "<option value='$row_branch[username]'>$row_branch[username]</option>"; 
					}
				}
				
			echo "
			</select>
<button type='button' $disable_transfer id='transfer_button$row[id]' onclick=Transfer('$row[id]','$branch')
class='btn btn-primary'><span style='font-size:px;' class='glyphicon glyphicon-share-alt'></span></button></div>";
			}
		
	echo "</td>";
	
	echo "<td>";
	
	if($row['active_login']=="1")
	{
		echo "
			<div class='form-inline'>
			<select class='form-control' style='width:120px;font-size:12px;height:30px;' id='login_active$row[id]'>";
			?>
			<option <?php if($row['login_type']=="1"){ echo "selected"; } ?> value="1_1">LR View Only</option>
			<option <?php if($row['login_type']=="2"){ echo "selected"; } ?> value="1_2">Full Access</option>
			<option <?php if($row['login_type']=="6"){ echo "selected"; } ?> value="1_6">Full Access W/O Diesel</option>
			<option <?php if($row['login_type']=="0"){ echo "selected"; } ?> value="0_0">Block Login</option>
			<option <?php if($row['login_type']=="3"){ echo "selected"; } ?> value="1_3">Help+Functions</option>
			<option <?php if($row['login_type']=="4"){ echo "selected"; } ?> value="1_4">e-Diary only</option>
			<option <?php if($row['login_type']=="5"){ echo "selected"; } ?> value="1_5">POD rcv only</option>
			<?php
			echo "	
			</select>
		<button type='button' $disable_tremiate id='login_active_button$row[id]' onclick=ActiveLogin('$row[id]','$branch')
		class='btn btn-primary'><span style='font-size:px;' class='glyphicon glyphicon-share-alt'></span></button></div>";
	}
	else
	{
		echo "
			<div class='form-inline'>
			<select class='form-control' style='width:120px;font-size:12px;height:30px;' id='login_active$row[id]'>";
			?>
			<option <?php if($row['login_type']=="1"){ echo "selected"; } ?> value="1_1">LR View Only</option>
			<option <?php if($row['login_type']=="2"){ echo "selected"; } ?> value="1_2">Full Access</option>
			<option <?php if($row['login_type']=="6"){ echo "selected"; } ?> value="1_6">Full Access W/O Diesel</option>
			<option <?php if($row['login_type']=="0"){ echo "selected"; } ?> disabled value="0_0">Login Blocked</option>
			<option <?php if($row['login_type']=="3"){ echo "selected"; } ?> value="1_3">Help+Functions</option>
			<option <?php if($row['login_type']=="4"){ echo "selected"; } ?> value="1_4">e-Diary only</option>
			<option <?php if($row['login_type']=="5"){ echo "selected"; } ?> value="1_5">POD rcv only</option>
			<?php
			echo "	
			</select>
		<button type='button' $disable_tremiate id='login_active_button$row[id]' onclick=ActiveLogin('$row[id]','$branch')
		class='btn btn-primary'><span style='font-size:px;' class='glyphicon glyphicon-share-alt'></span></button></div>";
	}
	
	echo "</td>";
	
	// echo "<td>
		// $diesel_entry_chk
	// </td>";
	
	echo "<td>";
	
	$alt_branch1="";
	
	if($row['alternate_branch']!='')
	{
		$alt_branch1=array();
	
		foreach(explode(",",$row['alternate_branch']) as $alt_branches){
			$alt_branch1[]="'".$alt_branches;
		}
		
		$alt_branch1=implode("',",$alt_branch1)."'";
		
		$alt_branch2=array();
		
		foreach(explode(",",$row['alternate_branch']) as $alt_branches2){
			$alt_branch2[]="<a href='#' onclick=RemoveAltBranch('$row[id]','$alt_branches2')>$alt_branches2</a>";
		}
		// trim(str_replace("SRL","",$title));
		if($row['code']!='032'){
			echo implode(" ",$alt_branch2);
		}
		else{
			echo "";
		}
		echo "<br>";
		echo "<br>";
	}
		
   echo "<div class='form-inline'>
		<select class='form-control' style='width:120px;font-size:12px;height:30px;' id='alt_branch_name$row[id]'>
		<option value=''>--branch--</option>"; 
		
		if($alt_branch1!=''){
			$get_alt_branch = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('$branch','HEAD','DUMMY') 
			AND username NOT IN($alt_branch1) ORDER by username ASC");
		}
		else{
			$get_alt_branch = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('$branch','HEAD','DUMMY') 
			ORDER by username ASC");
		}
		
		if(!$get_alt_branch){
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			Redirect("Error while processing Request","./");
			exit();
		}

		if(numRows($get_alt_branch)>0)
		{
			while($row_alt = fetchArray($get_alt_branch))
			{
				echo "<option value='$row_alt[username]'>$row_alt[username]</option>"; 
			}
		}
				
		echo "</select>
	<button type='button' $disable_alt_branch id='alt_button$row[id]' onclick=AltBranchFunc('$row[id]')
	class='btn btn-primary'><span style='font-size:px;' class='glyphicon glyphicon-share-alt'></span></button></div>";
	
	echo "</td>";
	
	?>
	<td>
	<div class="form-inline">
		<select class="form-control" style="width:120px;font-size:12px;height:30px;" id="whatsapp_enabled_<?php echo $row["id"]; ?>">
			<option <?php if($row["whatsapp_enabled"]=="1"){ echo "selected"; } ?> value="1">Yes</option>
			<option <?php if($row["whatsapp_enabled"]=="0"){ echo "selected"; } ?> value="0">No</option>
			</select>
		<button type="button" onclick="WhatsappToggle('<?php echo $row["id"]; ?>')" class="btn btn-primary"><span style="font-size:px;" class="glyphicon glyphicon-share-alt"></span></button>
	</div>
	</td>
	<?php
	
	echo "</td><td><button type='button' $disable_tremiate id='terminate_button$row[id]' onclick=Terminate('$row[id]','$branch') 
	class='btn btn-danger'><span class='glyphicon glyphicon-trash'></span></button></td>
	
	<td><button type='button' $disable_tremiate id='ResetPasswordBtn$row[id]' onclick=ResetPassword('$row[id]') class='btn btn-xs btn-danger'><i class='fa fa-refresh' aria-hidden='true'></i> Reset</button></td>
	</tr>";
		
		
	$sn++;	
	}
}
else
{
	echo "<tr><td colspan='8'>No records found.</td></tr>";
}
			?>			
			</table>
		</div>
		
	</div>

<div id="func_result22"></div>

<script>
function DieselEntryEd(id)
{
	$("#loadicon").show();
		jQuery.ajax({
		url: "./diesel_entry_toggle_save.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data){
			$("#func_result22").html(data);
		},
		error: function() {}
		});
}

function WhatsappToggle(id)
{
	var toggle = $('#whatsapp_enabled_'+id).val();
	
	$("#loadicon").show();
		jQuery.ajax({
		url: "./whatsapp_toggle.php",
		data: 'id=' + id + '&toggle=' + toggle,
		type: "POST",
		success: function(data){
			$("#func_result22").html(data);
		},
		error: function() {}
		});
}

function RemoveAltBranch(id,branch)
{
	if(confirm("are you sure to remove "+branch+" from alternate branch ?")==true)
	{
		$("#loadicon").show();
		jQuery.ajax({
		url: "./remove_alt_branch.php",
		data: 'id=' + id + '&branch=' + branch,
		type: "POST",
		success: function(data){
			$("#func_result22").html(data);
		},
		error: function() {}
		});
	}
}

function AltBranchFunc(id)
{
	var branch = $('#alt_branch_name'+id).val();
	
	if(branch!='')
	{
		if(confirm("are you sure to add "+branch+" as alternate branch ?")==true)
		{
			$('#alt_button'+id).attr('disabled',true);
			$('#alt_branch_name'+id).attr('disabled',true);
			
			$("#loadicon").show();
			jQuery.ajax({
			url: "./save_alt_branch.php",
			data: 'id=' + id + '&branch=' + branch,
			type: "POST",
			success: function(data){
				$("#func_result22").html(data);
			},
			error: function() {}
			});
		}
	}
}

	$('#loadicon').hide();
</script>