<?php
require_once './_connect.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$id = escapeString($conn,strtoupper($_POST['id']));
$branch = escapeString($conn,strtoupper($_POST['branch']));
$code = escapeString($conn,strtoupper($_POST['code']));

if($id==""){
	echo "<script>
		alert('Employee id not found !');
		window.location.href='./employee_management.php';
	</script>";
	exit();
}

$get_data = Qry($conn,"SELECT name,code,branch,mobile_no,status FROM emp_attendance WHERE id='$id'");

if(!$get_data){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_data)==0){
	Redirect("No result found.","./employee_management.php");
	exit();
}

$row_data = fetchArray($get_data);

if($row_data['code']!=$code)
{
	echo "<script>
		alert('Employee code not verified !');
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row_data['status']!="-1")
{
	echo "<script>
		alert('Not terminated employee !');
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row_data['branch']!=$branch)
{
	echo "<script>
		alert('Employee does not belongs to your Branch !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$emp_name = $row_data['name'];
$mobile_no = $row_data['mobile_no'];

if(strlen($mobile_no)!=10)
{
	echo "<script>
		alert('Update mobile number first !');
		$('#loadicon').hide();
		$('#active_btn$id').attr('disabled',false);
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$new_pass = GenPassword(8);

$msg="Hello $row_data[name], Welcome back to Raman Roadways. Your new login password is: $new_pass.";
// MsgEmpActiveAgain($row_data['mobile_no'],$row_data['name'],$new_pass);

$msg_template="Hello $row_data[name],\nWelcome back to RamanRoadways.\nYour new login password is: $new_pass.";
SendWAMsg($conn,$row_data['mobile_no'],$msg_template);

$update_emp = Qry($conn,"UPDATE emp_attendance SET password='".md5($new_pass)."',last_pass='$timestamp',active_login='1',status='3' 
WHERE id='$id'");

if(!$update_emp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$chk_Today_attd = Qry($conn,"SELECT id FROM emp_attd_check WHERE date='$date' AND branch='$branch' AND (p+a+hd)=0");
if(!$chk_Today_attd){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_Today_attd)>0)
{
	$row_attd = fetchArray($chk_Today_attd);
	
	$update_today_data = Qry($conn,"UPDATE emp_attd_check SET total=total+1 WHERE id='$row_attd[id]'");
	if(!$update_today_data){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,timestamp) VALUES 
('$code','Employee','Employee_Activated','Employee activated after termination name: $emp_name, Mobile: $mobile_no, code: $code.',
'ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Employee : $emp_name. Successfully Activated.');
		$('#emp_status_td$id').html('Activated');
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./employee_management.php");
	exit();
}
?>