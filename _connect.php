<?php
session_start();

if(!isset($_SESSION['rrpl_super_user']) AND !isset($_SESSION['user_rkg']))
{	
	echo "<script>
		window.location.href='/';
	</script>";
	exit();
}

if(isset($_SESSION['user_rkg']))
{
	$_SESSION['asset_manager_ho']=$_SESSION['user_rkg'];
}
else
{
	$_SESSION['asset_manager_ho']=$_SESSION['rrpl_super_user'];
}

include("../../_connect.php");

$conn=mysqli_connect($host,$username,$password,$db_name);

if(!$conn){
    echo "<span style='font-family:Verdana'>Database Connection failed: <b><font color='red'>" .mysqli_connect_error()."</b></font></span>";
	exit();
}

$page_name=$_SERVER['REQUEST_URI'];

$timestamp=date("Y-m-d H:i:s");

if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    }
    else{
        $protocol = 'http';
    }
	
$protocol = $protocol."://".$_SERVER['HTTP_HOST'];	
  // $baseUrl1 = $protocol."/b5aY6EZzK52NA8F/";
  // $url_of_diesel_card = $protocol."/b5aY6EZzK52NA8F/autofill/get_diesel_cards.php";
  // $url_of_diesel_submit = $protocol."/b5aY6EZzK52NA8F/add_diesel_adv.php";
  $url_of_icon = $protocol."/b5aY6EZzK52NA8F/load.gif";
?>
