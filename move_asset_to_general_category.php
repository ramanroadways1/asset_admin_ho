<?php
require_once './_connect.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$id = escapeString($conn,strtoupper($_POST['id']));
$asset_type = escapeString($conn,($_POST['asset_type']));

if($asset_type=='VEHICLE')
{
	$table_name = "asset_vehicle";
	$code = "reg_no";
	$general_specific = "asset_type";
	$vou_type_asset = "Asset_vehicle_move_to_general";
	$holder_var = "vehicle_holder";
}
else if($asset_type=='ASSET')
{
	$table_name = "asset_main";
	$code = "req_code";
	$general_specific = "typeof";
	$vou_type_asset = "Asset_move_to_general";
	$holder_var = "holder";
}
else
{
	echo "<script>
		alert('Invalid asset type.');
		$('#move_button$id').attr('disabled',true);	
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_asset = Qry($conn,"SELECT active,`$general_specific` as general_specific,branch,`$code` as code,`$holder_var` as asset_holder 
FROM `$table_name` WHERE id='$id'");
if(!$chk_asset){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_asset)==0){
	echo "<script>
		alert('Asset not found.');
		$('#move_button$id').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_asset = fetchArray($chk_asset);

$asset_branch = $row_asset['branch'];
$asset_code = $row_asset['code'];
$asset_holder = $row_asset['asset_holder'];

if($row_asset['active']!='1')
{
	echo "<script>
		alert('Asset is not active !');
		$('#move_button$id').attr('disabled',true);	
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row_asset['general_specific']!='SPECIFIC')
{
	echo "<script>
		alert('Asset type is not : SPECIFIC !');
		$('#move_button$id').attr('disabled',true);	
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$transfer_asset = Qry($conn,"UPDATE `$table_name` SET `$general_specific`='GENERAL',`$holder_var`=branch WHERE id='$id'");
if(!$transfer_asset){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_entry = Qry($conn,"INSERT INTO asset_type_change(asset_id,asset_type,asset_code,asset_branch,asset_holder,from_category,
to_category,timestamp) VALUES 
('$id','$asset_type','$asset_code','$asset_branch','$asset_holder','SPECIFIC','GENERAL','$timestamp')");

if(!$insert_entry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,edit_by,timestamp) VALUES 
('$asset_code','Asset_Category_Change','$vou_type_asset','Category update from SPECIFIC to GENERAL. Asset Holder : $asset_holder',
'ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Asset type udpated to : GENERAL !');
		$('#move_button$id').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();	
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}	
?>