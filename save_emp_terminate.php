<?php
require_once './_connect.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$id = escapeString($conn,strtoupper($_POST['id']));
$code = escapeString($conn,strtoupper($_POST['code']));
$branch = escapeString($conn,strtoupper($_POST['branch']));
$emp_name = escapeString($conn,strtoupper($_POST['emp_name']));

if($id==""){
	echo "<script>
		alert('Employee id not found !');
		window.location.href='./employee_management.php';
	</script>";
	exit();
}

$GetStatus = Qry($conn,"SELECT status FROM emp_attendance WHERE id='$id'");
if(!$GetStatus){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error !');
		window.location.href='./employee_management.php';
	</script>";
	exit();
}

if(numRows($GetStatus)==0)
{
	echo "<script>
		alert('Employee not found !');
		window.location.href='./employee_management.php';
	</script>";
	exit();
}

$row_ChkStatus = fetchArray($GetStatus);

if($row_ChkStatus['status']!=3)
{
	echo "<script>
		alert('Employee is in-active !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_manager = Qry($conn,"SELECT id FROM manager WHERE emp_code='$code'");
if(!$chk_manager){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	echo "<script>
		alert('Error !');
		window.location.href='./employee_management.php';
	</script>";
	exit();
}

if(numRows($chk_manager)>0)
{
	echo "<script>
		alert('$emp_name is manager of branch: $branch !');
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$approve = Qry($conn,"INSERT INTO emp_terminated(code,branch,approval,user_code,approval_timestamp,timestamp) VALUES 
('$code','$branch','1','ADMIN','$timestamp','$timestamp')");

if(!$approve){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$new_pass = GenPassword(8);

$update_emp = Qry($conn,"UPDATE emp_attendance SET password='".md5($new_pass)."',last_pass='$timestamp',active_login='0',status='-1',terminate='1' 
WHERE id='$id'");

if(!$update_emp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$chk_Today_attd = Qry($conn,"SELECT id FROM emp_attd_check WHERE date='$date' AND branch='$branch' AND (p+a+hd)=0");
if(!$chk_Today_attd){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if(numRows($chk_Today_attd)>0)
{
	$row_attd = fetchArray($chk_Today_attd);
	
	$update_today_data = Qry($conn,"UPDATE emp_attd_check SET total=total-1 WHERE id='$row_attd[id]'");
	if(!$update_today_data){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,timestamp) VALUES ('$code','Employee',
'Employee_Terminate','$emp_name Terminated from branch : $branch.','ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Employee : $emp_name. Successfully Terminated.');
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./employee_management.php");
	exit();
}
?>