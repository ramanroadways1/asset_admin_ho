<?php
require_once './_connect.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$id = escapeString($conn,($_POST['id']));
$elem = escapeString($conn,($_POST['elem']));

if($id==""){
	echo "<script>
		alert('Value not found !');
		window.location.href='./employee_management.php';
	</script>";
	exit();
}

if($elem==""){
	echo "<script>
		alert('Value not found !');
		window.location.href='./employee_management.php';
	</script>";
	exit();
}

$active_login = explode("_",$elem)[0];
$login_type = explode("_",$elem)[1];

StartCommit($conn);
$flag = true;

$log_data="Login";

if($active_login=="1")
{
	$log_data.=" Activated";
}
else
{
	$log_data.=" Blocked";
}

if($login_type=="0")
{
	$log_data.=", Login Role: None";
}
else if($login_type=="1")
{
	$log_data.=", Login Role: LR only";
}
else if($login_type=="3")
{
	$log_data.=", Login Role: Help+Func.";
}
else if($login_type=="4")
{
	$log_data.=", Login Role: e-Diary only";
}
else
{
	$log_data.=", Login Role: All Func.";
}

$update_emp = Qry($conn,"UPDATE emp_attendance SET active_login='$active_login',login_type='$login_type' WHERE id='$id'");

if(!$update_emp){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,timestamp) VALUES 
('$id','Employee','Employee_Login_Update','$log_data','ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Updated Successfully !!');
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./employee_management.php");
	exit();
}
?>