<?php
require_once './_connect.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

include ("./_header.php");
?>
<div class="container-fluid" style="color:#000">

<div class="row">
	<div class="from-group col-md-12" id="result_div"></div>
</div>

</div>
</body>

<script type="text/javascript">
function load_Approvals(){
	$('#loadicon').show();
    $.ajax({
    url: "load_vehicle_approvals.php",
    cache: false,
    success: function(data){
       $("#result_div").html(data);
	} 
  });
}

load_Approvals();
</script>

<script>
function DeleteReq(id)
{
	if(confirm("You you really want to delete this request ?")==true)
	{
		$('#DeleteReq'+id).attr('disabled',true);
		$('#ApproveAsset'+id).attr('disabled',true);
		
		$("#loadicon").show();
		jQuery.ajax({
		url: "./delete_asset_req.php",
		data: 'id=' + id + '&type=' + 'VEHICLE',
		type: "POST",
		success: function(data) {
			$("#func_result").html(data);
		},
		error: function() {}
		});
	}
}

function ApproveAsset(id)
{
	if(confirm("Are you sure ?")==true)
	{
		var company_name = $('#company_name_'+id).val();
		
		if(company_name=='')
		{
			alert('Select company first !');
		}
		else
		{
			$('#DeleteReq'+id).attr('disabled',true);
			$('#ApproveAsset'+id).attr('disabled',true);
			
			$("#loadicon").show();
			jQuery.ajax({ 
			url: "./save_vehicle_approval_new.php",
			data: 'id=' + id + '&company=' + company_name,
			type: "POST",
			success: function(data) {
				$("#func_result").html(data);
			},
			error: function() {}
			});
		}
	}
	
	// $('#loadicon').show();
	
	// $('#ApproveAsset'+id).attr('disabled',true);
	
	// var branch_user = $('#BranchUser1'+id).val();
	// var maker = $('#MakerName1'+id).val();
	// var model = $('#ModelName1'+id).val();
	// var vehicleCat = $('#Veh_type'+id).val();
	// var date = $('#ReqDate'+id).val();
	// var payment_mode = $('#payment_mode'+id).val();
	// var party_name = $('#legal_name'+id).val();
	// var party_id = $('#partyId'+id).val();
	// var pan_no = $('#pan_no'+id).val();
	// var gst_no = $('#gst_no'+id).val();
	// var ac_details = $('#ac_details'+id).val();
	// var asset_branch = $('#asset_branch'+id).val();
	
	// $('#veh_catagory').val(vehicleCat);
	// $('#modal_date').val(date);
	// $('#modal_username').val(branch_user);
	// $('#modal_maker').val(maker);
	// $('#modal_model').val(model);
	// $('#asset_id_tabs').val(id);
	// $('#modal_payment_mode').val(payment_mode);
	// $('#modal_party').val(party_name);
	// $('#modal_party_id').val(party_id);
	// $('#modal_party_pan').val(pan_no);
	// $('#modal_party_gst').val(gst_no);
	// $('#modal_ac_details').val(ac_details);
	// $('.branchDebit').html(asset_branch);
	
	// PaymentBy(payment_mode);	
	// $('#BtnAssetApproveModal').click(); 
	// $('#loadicon').hide();
}
</script>

<div id="func_result"></div>

<?php// include("./modal_approve_vehicle.php"); ?>
<?php //include("./modal_add_vehicle_party.php"); ?>