<?php
require_once './_connect.php';
?>
<div class="row">	

		<div class="form-group col-md-4">
			<h4 style="color:#000"><i class="fa fa-mobile" aria-hidden="true"></i> &nbsp; New Asset Requests :</h4> 
		</div>
		
	<div class="form-group col-md-12 table-responsive">
			<table class="table table-bordered table-striped" style="font-size:12px;">
				<tr style="background:#299C9B;font-size:13px;color:#FFF">
					<th>#</th>
					<th>Branch</th>
					<th>Token_No</th>
					<th>Catagory</th>
					<th>Date</th>
					<th>Username</th>
					<th>Maker & Model</th>
					<th>Narration</th>
					<th>Payment<br>Mode</th>
					<th>Amount</th>
					<th>GST_Invoice</th>
					<th>GST_Type</th>
					<th>GST(%)</th>
					<th>GST_Amount(+)</th>
					<th>Discount(-)</th>
					<th>Final_Amount</th>
					<th>Delete</th>
					<th>Approve</th>
				</tr>	
<?php
$getAsset = Qry($conn,"SELECT a.id,a.req_code,a.date,a.branch,a.branch_user,a.maker,a.model,a.narration,a.payment_mode,c.title,u.name,
party.id as partyId,party.party_name,party.pan_no,party.gst_no,a.payment_amount,a.amount,a.gst_invoice,a.gst_type,a.gst_value,a.gst_amount,
a.discount,CONCAT('Ac Holder: ',party.ac_holder,', A/c No: ',party.ac_no,', Bank: ',party.bank_name,', IFSC: ',party.ifsc_code) as ac_details 
FROM asset_request AS a 
LEFT OUTER JOIN asset_category as c ON c.id=a.category
LEFT OUTER JOIN emp_attendance AS u ON u.code=a.branch_user 
LEFT OUTER JOIN asset_party AS party ON party.id=a.party_id 
WHERE a.ho_approval!='1' AND a.approval='1'");

if(!$getAsset){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($getAsset)>0)
{
	$sn1=1;
	while($row = fetchArray($getAsset))
	{
		if($row['branch']==$row['branch_user']){
			$branch_user = $row['branch'];
		}
		else{
			$branch_user = $row['name'];
		}
		
		$ReqDate = convertDate("d-m-y",$row["date"]);
		
		echo "<tr>
			<td>$sn1</td>
			<td>$row[branch]</td>
			<td>$row[req_code]</td>
			<td>$row[title]</td>
			<td>$ReqDate</td>
			<td>$branch_user</td>
			<td>$row[maker] - $row[model]</td>
			<td>$row[narration]</td>
			<td>$row[payment_mode]</td>
			<td>$row[amount]</td>
			<td>$row[gst_invoice]</td>
			<td>$row[gst_type]</td>
			<td>$row[gst_value]</td>
			<td>$row[gst_amount]</td>
			<td>$row[discount]</td>
			<td>$row[payment_amount]</td>
			<td><button type='button' id='DeleteReq$row[id]' onclick='DeleteReq($row[id])'  
				class='btn btn-xs btn-danger'><span class='glyphicon glyphicon-trash'></span> Reject</a></td>
			<td>
				<input type='hidden' id='BranchUser1$row[id]' value='$branch_user'>
				<input type='hidden' id='MakerName1$row[id]' value='$row[maker]'>
				<input type='hidden' id='ModelName1$row[id]' value='$row[model]'>
				<input type='hidden' id='AssetCat1$row[id]' value='$row[title]'>
				<input type='hidden' id='ReqDate$row[id]' value='$ReqDate'>
				<input type='hidden' id='payment_mode$row[id]' value='$row[payment_mode]'>
				<input type='hidden' id='partyId$row[id]' value='$row[partyId]'>
				<input type='hidden' id='legal_name$row[id]' value='$row[party_name]'>
				<input type='hidden' id='pan_no$row[id]' value='$row[pan_no]'>
				<input type='hidden' id='gst_no$row[id]' value='$row[gst_no]'>
				<input type='hidden' id='ac_details$row[id]' value='$row[ac_details]'>
				<input type='hidden' id='asset_branch$row[id]' value='$row[branch]'>
				<select id='company_name_$row[id]' style='font-size:11px !important'>
					<option value=''>--select company--</option>
					<option style='font-size:11px !important' value='RRPL'>RRPL</option>
					<option style='font-size:11px !important' value='RAMAN_ROADWAYS'>RAMAN_ROADWAYS</option>
				</select>
				<br />
				<br />
				<button type='button' id='ApproveAsset$row[id]' onclick='ApproveAsset($row[id])' 
				class='btn btn-xs btn-success'><span class='glyphicon glyphicon-thumbs-up'></span> Approve</a>
			</td>
		</tr>";
		
	$sn1++;	
	}
}
else
{
	echo "<tr><td colspan='15'>No records found.</td></tr>";
}
			?>			
			</table>
		</div>
		
	</div>
	
<script>
	$('#loadicon').hide();
</script>	