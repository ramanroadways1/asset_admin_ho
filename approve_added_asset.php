<?php
require_once './_connect.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$id = escapeString($conn,strtoupper($_POST['id']));

$chk_veh = Qry($conn,"SELECT id,ho_approval FROM asset_main WHERE id='$id'");
if(!$chk_veh){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_veh)==0){
	echo "<script>
		alert('Request not found.');
		$('#ApproveAsset$id').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$approve = Qry($conn,"UPDATE asset_main SET active='1',ho_approval='1',ho_approval_time='$timestamp' WHERE id='$id'");

if(!$approve){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		$('#ApproveAsset$id').attr('disabled',true);
		$('#ApproveAsset$id').html('Approved');
		$('#loadicon').hide();
	</script>";
	exit();	
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./asset_approval_after_added.php");
	exit();
}	
?>