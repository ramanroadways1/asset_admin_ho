<?php
require_once './_connect.php';

$branch = escapeString($conn,strtoupper($_POST['branch']));
$code = escapeString($conn,strtoupper($_POST['code']));
$mobile = escapeString($conn,strtoupper($_POST['mobile']));
$emp_id = escapeString($conn,strtoupper($_POST['emp_id']));
$timestamp = date("Y-m-d H:i:s");

if($code==''){
	echo "<script>
		alert('Employee code is empty !');
		$('#loadicon').hide();
	</script>";
	exit();
}

if($mobile==''){
	echo "<script>
		alert('Employee mobile number is not valid !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_employee = Qry($conn,"SELECT name,mobile_no,code FROM emp_attendance WHERE id='$emp_id'");
if(!$chk_employee){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

$row_check = fetchArray($chk_employee);

if($row_check['mobile_no']!=$mobile)
{
	echo "<script>
		alert('Mobile number not verified !');
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row_check['code']!=$code)
{
	echo "<script>
		alert('Employee code not verified !');
		$('#loadicon').hide();
	</script>";
	exit();
}

$emp_name = $row_check['name'];

$password = GenPassword(8);
MsgResetManagerPassword($mobile,$password,$branch,$emp_name);

StartCommit($conn);
$flag = true;

$updatePass = Qry($conn,"UPDATE manager SET pass='".md5($password)."',last_pass='$timestamp' WHERE branch='$branch'");
if(!$updatePass){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,timestamp) VALUES ('$code','Manager',
'Password_reset','Manager password reseted by admin.','ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}


if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Password reset success!');
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./branch_managers.php");
	exit();
}
	
?>