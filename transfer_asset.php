<?php
require_once './_connect.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

$id = escapeString($conn,strtoupper($_POST['id']));
$branch = escapeString($conn,strtoupper($_POST['branch']));
$asset_type = escapeString($conn,($_POST['asset_type']));

if($asset_type=='VEHICLE')
{
	$table_name = "asset_vehicle";
	$general_specific = "asset_type";
	$code = "reg_no";
	$vou_type_asset = "Asset_vehicle_transfer";
}
else if($asset_type=='ASSET')
{
	$table_name = "asset_main";
	$general_specific = "typeof";
	$code = "req_code";
	$vou_type_asset = "Asset_transfer";
}
else
{
	echo "<script>
		alert('Invalid asset type.');
		$('#transfer_button$id').attr('disabled',true);	
		$('#loadicon').hide();
	</script>";
	exit();
}

$chk_asset = Qry($conn,"SELECT active,`$general_specific` as general_specific,branch,`$code` as code FROM `$table_name` 
WHERE id='$id'");
if(!$chk_asset){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_asset)==0){
	echo "<script>
		alert('Asset not found.');
		$('#transfer_button$id').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}

$row_asset = fetchArray($chk_asset);

$from_branch = $row_asset['branch'];
$asset_code = $row_asset['code'];

if($row_asset['active']!='1')
{
	echo "<script>
		alert('Asset is not active !');
		$('#transfer_button$id').attr('disabled',true);	
		$('#loadicon').hide();
	</script>";
	exit();
}

if($row_asset['general_specific']=='SPECIFIC')
{
	echo "<script>
		alert('SPECIFIC asset is not transferable !');
		$('#transfer_button$id').attr('disabled',true);	
		$('#loadicon').hide();
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$transfer_asset = Qry($conn,"UPDATE `$table_name` SET branch='$branch' WHERE id='$id'");
if(!$transfer_asset){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_entry = Qry($conn,"INSERT INTO asset_transfer(asset_id,asset_type,asset_code,from_branch,to_branch,timestamp) VALUES 
('$id','$asset_type','$asset_code','$from_branch','$branch','$timestamp')");

if(!$insert_entry){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,edit_by,timestamp) VALUES 
('$asset_code','Asset_Transfer','$vou_type_asset','Asset Transfer from $from_branch to $branch Branch.','ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Asset Transfer to : $branch Branch !');
		$('#asset_transfer_branch$id').attr('disabled',true);
		$('#transfer_button$id').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();	
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}	
?>