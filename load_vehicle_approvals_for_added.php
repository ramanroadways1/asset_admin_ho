<?php
require_once './_connect.php';
?>
<div class="row">	

	<div class="form-group col-md-4">
		<h4 style="color:#000"><i class="fa fa-car" aria-hidden="true"></i> &nbsp; Added Vehicle Approval :</h4> 
	</div>
		
	<div class="form-group col-md-12 table-responsive">
			<table class="table table-bordered table-striped" style="font-size:12px;">
				<tr style="background:#299C9B;font-size:13px;color:#FFF">
					<th>#</th>
					<th>Vehicle_Holder</th>
					<th>New/Old</th>
					<th>Token_No</th>
					<th>Reg.No</th>
					<th>Branch</th>
					<th>Branch_User</th>
					<th>Gen./Specific</th>
					<th>Veh_Type</th>
					<th>Reg.Date</th>
					<th>Owner_Name</th>
					<th>Veh_Class</th>
					<th>Fuel_Type</th>
					<th>Chasis_No</th>
					<th>Engine_No</th>
					<th>Maker</th>
					<th>Model</th>
					<th>Rc</th>
					<th>Insurance</th>
					<th>Puc</th>
					<th>Amount</th>
					<th>Gst_Amt</th>
					<th>Total_Amt</th>
					<th>Approve</th>
				</tr>	
<?php
$getAsset = Qry($conn,"SELECT a.id,a.token_no,a.vehicle_holder,a.reg_no,a.branch,a.asset_type,a.veh_type,a.reg_date,
a.owner_name,a.veh_class,a.chasis_no,a.engine_no,a.maker,a.model,a.rc,a.insurance,a.puc,a.fuel_type,a.date,a.invoice,a.amount,
a.gst_amount,a.total_amount,a.timestamp,a.add_type,e.name,e2.name as veh_holder 
FROM asset_vehicle AS a 
LEFT OUTER JOIN emp_attendance as e ON e.code=a.branch_user
LEFT OUTER JOIN emp_attendance as e2 ON e2.code=a.vehicle_holder
WHERE a.ho_approval=0");

if(!$getAsset){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($getAsset)>0)
{
	$sn1=1;
	while($row = fetchArray($getAsset))
	{
		if($row['add_type']=='2'){
			$vehicle_new = "<font color='black'>Old</font>";
		}else{
			$vehicle_new = "<font color='red'>New</font>";
		}
		
		if($row['rc']=='1'){
			$rc_status = "<font color='green'>Updated</font>";
		}else{
			$rc_status = "<font color='red'>Pending</font>";
		}
		
		if($row['insurance']=='1'){
			$ins_status = "<font color='green'>Updated</font>";
		}else{
			$ins_status = "<font color='red'>Pending</font>";
		}
		
		if($row['puc']=='1'){
			$puc_status = "<font color='green'>Updated</font>";
		}else{
			$puc_status = "<font color='red'>Pending</font>";
		}
		
		if($row['branch']==$row['vehicle_holder']){
			$vehicle_holder = $row['branch'];
		}
		else{
			$vehicle_holder = $row['veh_holder'];
		}
		
		$ReqDate = convertDate("d-m-y",$row["date"]);
		
		echo "<tr>
			<td>$sn1</td>
			<td>$vehicle_holder</td>
			<td>$vehicle_new</td>
			<td>$row[token_no]</td>
			<td>$row[reg_no]</td>
			<td>$row[branch]</td>
			<td>$row[name]</td>
			<td>$row[asset_type]</td>
			<td>$row[veh_type]</td>
			<td>$ReqDate</td>
			<td>$row[owner_name]</td>
			<td>$row[veh_class]</td>
			<td>$row[fuel_type]</td>
			<td>$row[chasis_no]</td>
			<td>$row[engine_no]</td>
			<td>$row[maker]</td>
			<td>$row[model]</td>
			<td>$rc_status</td>
			<td>$ins_status</td>
			<td>$puc_status</td>
			<td>$row[amount]</td>
			<td>$row[gst_amount]</td>
			<td>$row[total_amount]</td>
			<td>
				<button type='button' id='ApproveAsset$row[id]' onclick='ApproveAsset($row[id])' 
				class='btn btn-sm btn-success'><span class='glyphicon glyphicon-thumbs-up'></span> Approve</a>
			</td>
		</tr>";
		
	$sn1++;	
	}
}
else
{
	echo "<tr><td colspan='30'>No records found.</td></tr>";
}
			?>			
			</table>
		</div>
		
	</div>
	
<script>
	$('#loadicon').hide();
</script>	