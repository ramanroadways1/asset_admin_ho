<?php
require_once './_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$id = escapeString($conn,strtoupper($_POST['id']));
$company = escapeString($conn,strtoupper($_POST['company']));

$get_request = Qry($conn,"SELECT a.req_code,a.branch,a.narration,a.payment_mode,a.cheque_no,a.payment_amount,p.mobile,p.gst_no,p.pan_no,
p.ac_holder,p.ac_no,p.bank_name,p.ifsc_code,a.gst_invoice,a.ho_approval,c.title as asset_category,m.emp_code,u.mobile_no as manager_mobile,
a.maker,a.model
FROM asset_request AS a 
LEFT OUTER JOIN asset_category AS c ON c.id = a.category 
LEFT OUTER JOIN asset_party AS p ON p.id = a.party_id 
LEFT OUTER JOIN manager AS m ON m.branch = a.branch 
LEFT OUTER JOIN emp_attendance AS u ON u.code = m.emp_code 
WHERE a.id='$id'");

if(!$get_request){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_request)==0)
{
	echo "<script>
		alert('Asset request not found !');
		$('#loadicon').fadeOut('slow');
		$('#ApproveAsset$id').attr('disabled', false);
	</script>";
	exit();
}

$row_data = fetchArray($get_request);

$maker = $row_data['maker'];
$model = $row_data['model'];
$manager_mobile = $row_data['manager_mobile'];
$req_code = $row_data['req_code'];
$asset_category = $row_data['asset_category'];
$mobile = $row_data['mobile'];
$acname = $row_data['ac_holder'];
$acno = $row_data['ac_no'];
$bank_name = $row_data['bank_name'];
$ifsc = $row_data['ifsc_code'];
$pan = $row_data['pan_no'];
$gst_no = $row_data['gst_no'];
$payment_mode = $row_data['payment_mode'];
$total_amount = $row_data['payment_amount'];
$cheque_no = $row_data['cheque_no'];

if($row_data['gst_invoice']=='YES' AND $gst_no=='')
{
	echo "<script>
		alert('GST number not found !');
		$('#loadicon').fadeOut('slow');
		$('#ApproveAsset$id').attr('disabled', false);
	</script>";
	exit();
}

if($payment_mode=='NEFT' AND ($acname=='' || $acno==''))
{
	echo "<script>
		alert('Invalid account details !');
		$('#loadicon').fadeOut('slow');
		$('#ApproveAsset$id').attr('disabled', false);
	</script>";
	exit();
}

if($row_data['ho_approval']!=0)
{
	echo "<script>
		alert('Asset Request already approved.');
		$('#loadicon').fadeOut('slow');
		$('#ApproveAsset$id').attr('disabled', true);
	</script>";
	exit();
}

$asset_branch = $row_data['branch'];

if($payment_mode=='CASH' AND $total_amount>10000)
{
	echo "<script>
		alert('Max cash allowed is 10,000 !');
		$('#ApproveAsset$id').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

$vou_no_Qry = GetVouId("E",$conn,"mk_venf","vno",$asset_branch);
	
if(!$vou_no_Qry || $vou_no_Qry=="0" || $vou_no_Qry==''){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}
		
$expid = $vou_no_Qry;
	
$chk_vou_no = Qry($conn,"SELECT id FROM mk_venf WHERE vno='$expid'");

if(!$chk_vou_no){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}
		
if(numRows($chk_vou_no)>0)
{
	echo "<script>
		alert('Duplicate Voucher No: $expid. Try again !');
		$('#ApproveAsset$id').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}	

$check_balance = Qry($conn,"SELECT balance,balance2,email FROM user WHERE username='$asset_branch'");

if(!$check_balance){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}
	
if(numRows($check_balance)==0)
{
	echo "<script>
		alert('Branch $asset_branch not found !');
		$('#ApproveAsset$id').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
	
$row_balance = fetchArray($check_balance);
$rrpl_balance = $row_balance['balance'];
$rr_balance = $row_balance['balance2'];
$email = $row_balance['email'];
	
if($payment_mode=='CASH')
{	
	if($company=='RRPL' && $rrpl_balance>=$total_amount)
	{
		$new_amount = $rrpl_balance-$total_amount;
		$balance_col="balance";
		$debit_col="debit";
	}
	else if($company=='RAMAN_ROADWAYS' && $rr_balance>=$total_amount)
	{
		$new_amount = $rr_balance-$total_amount;
		$balance_col="balance2";
		$debit_col="debit2";
	}
	else
	{
		echo "<script>
			alert('Insufficient balance in company : $company($asset_branch).');
			$('#ApproveAsset$id').attr('disabled',false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
	}
}

	if($company=='RRPL')
	{
		$balance_col="balance";
		$debit_col="debit";
	}
	else if($company=='RAMAN_ROADWAYS')
	{
		$balance_col="balance2";
		$debit_col="debit2";
	}
	else
	{
		echo "<script>
			alert('Invalid company : $company.');
			window.location.href='./vehicle_approval.php';
		</script>";
		exit();
	}	
	
StartCommit($conn);
$flag = true;

if($payment_mode=='CASH')
{
	$update_balance = Qry($conn,"update user set `$balance_col`='$new_amount' where username='$asset_branch'");
	if(!$update_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$insert_cash = Qry($conn,"INSERT INTO cashbook(user,date,vou_date,comp,vou_no,vou_type,desct,`$debit_col`,`$balance_col`,timestamp) 
	VALUES ('$asset_branch','$date','$date','$company','$expid','Expense_Voucher','NEW_ASSET','$total_amount','$new_amount','$timestamp')");

	if(!$insert_cash){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($payment_mode=='CHEQUE')
{
	$insert_passbook = Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,chq_no,`$debit_col`,timestamp) VALUES 
	('$asset_branch','$expid','$date','$date','$company','Expense_Voucher','NEW_ASSET','$cheque_no','$total_amount','$timestamp')");
	
	if(!$insert_passbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
$insert_cheque_book=Qry($conn,"INSERT INTO cheque_book(vou_no,vou_type,vou_date,amount,cheq_no,date,branch,timestamp) VALUES 
('$expid','Expense_Voucher','$date','$total_amount','$cheque_no','$date','$asset_branch','$timestamp')");

	if(!$insert_cheque_book){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($payment_mode=='NEFT')
{
	$chk_neft = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$expid'");
	if(!$chk_neft){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($company=='RRPL')
	{
		$get_Crn = GetCRN("RRPL-E",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		$crnnew = $get_Crn;
	}
	else
	{
		$get_Crn = GetCRN("RR-E",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		$crnnew = $get_Crn;
	}
	
	if(numRows($chk_neft)==0)		
	{
		$qry_rtgs=Qry($conn,"INSERT INTO rtgs_fm(fno,branch,com,totalamt,amount,acname,acno,bank_name,ifsc,pan,pay_date,fm_date,type,type2,
		email,crn,approval,timestamp_approve,timestamp) VALUES ('$expid','$asset_branch','$company','$total_amount','$total_amount','$acname','$acno','$bank_name',
		'$ifsc','$pan','$date','$date','EXPENSE_VOU','NEW_ASSET','$email','$crnnew','1','$timestamp','$timestamp')");			

		if(!$qry_rtgs){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$flag = false;
		errorLog("Duplicate Rtgs Voucher : $expid",$conn,$page_name,__LINE__);
	}
}
else
{
	$flag = false;
	errorLog("Invalid payment mode selected.",$conn,$page_name,__LINE__);
}	
	
$insert_voucher = Qry($conn,"INSERT INTO mk_venf(user,branch_user,vno,newdate,date,comp,des,desid,asset_voucher,amt,amt_w,chq,chq_no,chq_bnk_n,
neft_bank,neft_acname,neft_acno,neft_ifsc,pan,narrat,empcode,vehno,cash_sign,rcvr_sign,timestamp,upload) VALUES ('$asset_branch','CGROAD','$expid',
'$date','$date','$company','NEW_ASSET','','1','$total_amount','','$payment_mode','$cheque_no','','$bank_name','$acname','$acno',
'$ifsc','$pan','NEW Asset Purchase : Maker: $maker, Model: $model.','','','','','$timestamp','')");

if(!$insert_voucher){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}

$TodayData = Qry($conn,"UPDATE today_data SET exp_vou=exp_vou+1,exp_vou_amount=exp_vou_amount+'$total_amount' WHERE branch='$asset_branch'");

if(!$TodayData){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}
	
$save_approval = Qry($conn,"UPDATE asset_request SET vou_no='$expid',ho_approval='1',ho_approval_time='$timestamp' WHERE id='$id'");

if(!$save_approval){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,timestamp) VALUES ('$req_code','Asset',
'Approve','Asset Request Approved.','ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$msg_template = "*Asset Request Approved By Head-Office*\nAsset Category: $asset_category\nRequest Id: $req_code\nPayment Method: $payment_mode\nApproved At: $timestamp\nExp.Vou Id: $expid\nCompany: $company\nApproved Amount: $total_amount.";

SendWAMsg($conn,$manager_mobile.",9024281599",$msg_template);

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Asset Request : $req_code. Approved Successfully !');
		$('#mainModalHide2').click();
		$('#ApproveAsset$id').html('Approved');
		
		$('#DeleteReq$id').attr('disabled', true);
		$('#ApproveAsset$id').attr('disabled', true);
		
		$('#ApproveAsset$id').attr('onclick', '');
		$('#DeleteReq$id').attr('onclick', '');
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	
	echo "<script>
		alert('Error While Processing Request !');
		$('#loadicon').fadeOut('slow');
		$('#ApproveAsset$id').attr('disabled', false);
	</script>";
	
	exit();
}
?>