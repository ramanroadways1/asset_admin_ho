<?php
require_once './_connect.php';

$date = date("Y-m-d"); 

$total_emp = Qry($conn,"SELECT id FROM emp_attendance WHERE status IN('3','2','0') AND branch!=''");
$total_emp = numRows($total_emp);

$total_vehicle = Qry($conn,"SELECT id FROM asset_vehicle WHERE active='1'");
$total_vehicle = numRows($total_vehicle);

$total_asset = Qry($conn,"SELECT id FROM asset_main WHERE active='1'");
$total_asset = numRows($total_asset);

echo "<script>
	$('#TotalEmpSpan').html('$total_emp');
	$('#TotalAssetSpan').html('$total_asset');
	$('#TotalVehSpan').html('$total_vehicle');
	$('#loadicon').fadeOut();
</script>";
?>