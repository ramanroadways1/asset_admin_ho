<?php
require_once './_connect.php';

$date = date("Y-m-d"); 
$timestamp = date("Y-m-d H:i:s"); 

include ("./_header.php");

$report_name = escapeString($conn,$_REQUEST['report_name']);
?>
<div class="container-fluid" style="color:#000">

<div class="row">
	<div class="from-group col-md-12" id="result_div"></div>
</div>

</div>
</body>

<script type="text/javascript">
function load_report(report_name)
{
	jQuery.noConflict();
	$("#loadicon").show();
		jQuery.ajax({
			url: "_load_report.php",
			data: 'report_name=' + report_name,
			type: "POST",
			success: function(data) {
			$("#result_div").html(data);
			   $('#example').DataTable({ 
			   // "scrollY": 500,
				"scrollX": true,
				"lengthMenu": [ [15, 100,500, 1000, -1], [15, 100,500, 1000, "All"] ], 
				"bProcessing": true,
				"sPaginationType":"full_numbers",
				"dom": "lBfrtip",
				buttons: [
					'copyHtml5',
					'excelHtml5',
					// 'csvHtml5',
					// 'pdfHtml5'
				],
                 "destroy": true, //use for reinitialize datatable
				
            });
		},
		error: function() {}
	});
}
load_report('<?php echo $report_name; ?>');

function Transfer_Asset(id,asset_type,asset_typeof)
{
	var asset_typeof = asset_typeof.toUpperCase();
	var branch = $('#asset_transfer_branch'+id).val();
	
	if(confirm("Confirm transfer of asset ?")==true)
	{
		if(branch=='')
		{
			alert('select branch first !');
		}	
		else if(asset_typeof=='GENERAL' || asset_typeof=='SPECIFIC')
		{
			$('#transfer_button'+id).attr('disabled',true);	
			$("#loadicon").show();
				jQuery.ajax({
				url: "transfer_asset.php",
				data: 'id=' + id + '&asset_type=' + asset_type + '&branch=' + branch,
				type: "POST",
				success: function(data) {
				$("#func_result").html(data);
				},
			error: function() {}
			});
		}
		else
		{
			alert('asset type should be general or specific !');
		}
	}
}

function Move_Asset(id,asset_type,asset_typeof)
{
	var asset_typeof = asset_typeof.toUpperCase();
	
	if(confirm("Confirm transfer to general category ?")==true)
	{
		if(asset_typeof=='GENERAL' || asset_typeof=='SPECIFIC')
		{
			
			$('#move_button'+id).attr('disabled',true);	
					$("#loadicon").show();
						jQuery.ajax({
						url: "move_asset_to_general_category.php",
						data: 'id=' + id + '&asset_type=' + asset_type,
						type: "POST",
						success: function(data) {
						$("#func_result").html(data);
						},
					error: function() {}
				});
		}
		else
		{
			alert('asset type should be general or specific !');
		}
	}
}
</script>

<div id="func_result"></div>