<?php
require_once './_connect.php';

$gst = escapeString($conn,strtoupper($_POST['gst']));

$chkGst = Qry($conn,"SELECT legal_name FROM asset_party WHERE gst_no='$gst'");
if(!$chkGst){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chkGst)>0)
{
	echo "<script>
		alert('Duplicate GST Number : $gst.');
		$('#new_party_legal_name').val('');
		$('#new_party_pan_no').val('');
		$('#modal_gstno').attr('readonly',false);
		$('#ValidateBtn').attr('disabled',false);
		$('#buttonPartyADD').attr('disabled',true);
		$('#loadicon').hide();
	</script>";
	exit();
}
			
	$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.taxprogsp.co.in/commonapi/v1.1/search?aspid=$tax_pro_asp_id&password=$tax_pro_asp_password&Action=TP&Gstin=24AAGCR0742P1Z5&SearchGstin=$gst",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 900,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
			"Accept: */*",
			"Accept-Encoding: gzip, deflate",
			"Cache-Control: no-cache",
			"Connection: keep-alive",
			"Host: api.taxprogsp.co.in",
			"Postman-Token: fbf12a45-9e77-4846-a07e-aa599957d3ed,cc3bfe12-9401-47f6-a573-e12a12761576",
			"User-Agent: PostmanRuntime/7.15.2",
			"cache-control: no-cache"
		  ),
		));

		$result = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		
		if($err)
		{
			echo "<script>
				alert('Unable to Get GST Details from Server.');
					$('#new_party_legal_name').val('');
					$('#new_party_pan_no').val('');
					$('#modal_gstno').attr('readonly',false);
					$('#ValidateBtn').attr('disabled',false);
					$('#buttonPartyADD').attr('disabled',true);
				$('#loadicon').hide();
			</script>";
			exit();
		}
		else
		{
			$response = json_decode($result, true);
			
			if(@$response['error'])
			{
				$errMsg = $response['error']['message'];
				echo "<script>
					alert('Error : $errMsg.');
					$('#new_party_legal_name').val('');
					$('#new_party_pan_no').val('');
					$('#modal_gstno').attr('readonly',false);
					$('#ValidateBtn').attr('disabled',false);
					$('#buttonPartyADD').attr('disabled',true);
					$('#loadicon').hide();
				</script>";
				exit();
			}
			else
			{
				$LegalName = $response['lgnm'];
				$trade_name = $response['tradeNam'];
				$Pan_No = substr($gst,2,-3);
				
				if($LegalName=='' AND $trade_name=='')
				{
					echo "<script>
						alert('Party not found !');
						window.location.href='./';
					</script>";
					exit();
				}
				
				if($trade_name=='')
				{
					$trade_name = $LegalName;
				}
				
				if($LegalName=='')
				{
					$LegalName = $trade_name;
				}
			}
			
			echo "<script>
				$('#new_party_legal_name').val('$LegalName');
				$('#new_party_pan_no').val('$Pan_No');
				$('#modal_gstno').attr('readonly',true);
				$('#ValidateBtn').attr('disabled',true);
				$('#buttonPartyADD').attr('disabled',false);
				$('#loadicon').hide();
			</script>";
			exit();
		}	
?>