<?php
require_once './_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$id=escapeString($conn,strtoupper($_POST['id']));
$type=escapeString($conn,($_POST['type']));

if($type=='VEHICLE')
{
	$table="asset_vehicle_req";
	$date_col="req_date";
	$maker="maker_name";
	$model="model_name";
	$vou_type="ASSET_VEH_REQ";
}
else if($type=='ASSET')
{
	$table="asset_request";
	$date_col="date";
	$maker="maker";
	$model="model";
	$vou_type="ASSET_REQ";
}
else
{
	echo "<script>
		alert('Invalid asset type !');
		window.location.href='./';
	</script>";
	exit();
}

$chk_req = Qry($conn,"SELECT req_code,`$date_col` as req_date,`$maker` as maker_name,`$model` as model_name,branch,branch_user,ho_approval 
FROM `$table` WHERE id='$id'");

if(!$chk_req){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($chk_req)==0)
{
	echo "<script>
		alert('Request not found !');
		$('#loadicon').hide();
		$('#DeleteReq$id').attr('disabled', false);
		$('#ApproveAsset$id').attr('disabled', false);
	</script>";
	exit();
}

$row = fetchArray($chk_req);

$req_code = $row['req_code'];
$req_date = $row['req_date'];
$branch_user = $row['branch_user'];
$asset_branch = $row['branch'];
$maker_name = $row['maker_name'];
$model_name = $row['model_name'];

if($row['ho_approval']=="1")
{
	echo "<script>
		alert('Request already approved by head-office !');
		$('#loadicon').hide();
		$('#DeleteReq$id').attr('disabled', true);
		$('#ApproveAsset$id').attr('disabled', true);
	</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$edit_desc = "ReqCode : $req_code, Date: $req_date, Branch: $asset_branch, Maker: $maker_name, Model: $model_name, 
Branch_user: $branch_user.";

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,timestamp) VALUES ('$req_code','$vou_type',
'REQ_DEL','$edit_desc','ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}
	
$del_req = Qry($conn,"DELETE FROM `$table` WHERE id='$id'");

	if(!$del_req){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Asset Request Deleted Successfully !');
		$('#DeleteReq$id').html('Deleted');
		$('#DeleteReq$id').attr('disabled', true);
		$('#ApproveAsset$id').attr('disabled', true);
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./");
	exit();
}
?>