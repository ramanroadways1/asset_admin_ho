<?php
require_once './_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$req_id=escapeString($conn,strtoupper($_POST['req_id']));
$party_id=escapeString($conn,strtoupper($_POST['party_id']));
$maker=escapeString($conn,strtoupper($_POST['maker']));
$model=escapeString($conn,strtoupper($_POST['model']));
$party=escapeString($conn,strtoupper($_POST['party']));
$payment_mode=escapeString($conn,strtoupper($_POST['payment_mode']));
$asset_amount=escapeString($conn,strtoupper($_POST['asset_amount']));
$company=escapeString($conn,strtoupper($_POST['company']));
$cheque_no=escapeString($conn,strtoupper($_POST['cheque_no']));

$is_gst_invoice = escapeString($conn,($_POST['gst_selection']));
$gst_type = escapeString($conn,($_POST['gst_type']));
$gst_value = escapeString($conn,($_POST['gst_value']));
$gst_amount = escapeString($conn,($_POST['gst_amount']));
$discount = escapeString($conn,($_POST['discount']));
$total_amount = escapeString($conn,($_POST['total_amount']));

$VerifyParty = Qry($conn,"SELECT legal_name,mobile,gst_no,pan_no,ac_holder,ac_no,bank_name,ifsc_code FROM asset_party WHERE 
id='$party_id'");

if(!$VerifyParty){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($VerifyParty)==0)
{
	echo "<script>
		alert('Party not found.');
		$('#loadicon').hide();
		$('#buttonApprove1').attr('disabled', false);
	</script>";
	exit();
}

$rowParty = fetchArray($VerifyParty);

$mobile = $rowParty['mobile'];
$acname = $rowParty['ac_holder'];
$acno = $rowParty['ac_no'];
$bank_name = $rowParty['bank_name'];
$ifsc = $rowParty['ifsc_code'];
$pan = $rowParty['pan_no'];

if($is_gst_invoice=='YES')
{
	if($rowParty['gst_no']=='')
	{
		echo "<script>
			alert('GST not found !');
			$('#loadicon').hide();
			$('#buttonApprove1').attr('disabled', false);
		</script>";
		exit();
	}
	
	if(substr($rowParty['gst_no'], 0, 2)=="24"){
		$gst_type_db = "CGST+SGST";
	}
	else{
		$gst_type_db = "IGST";
	}
	
	if($gst_type_db!=$gst_type)
	{
		echo "<script>
			alert('GST type not verified !');
			$('#loadicon').hide();
			$('#buttonApprove1').attr('disabled', false);
		</script>";
		exit();
	}
	
	$gst_amount_db = sprintf("%.2f",($asset_amount*$gst_value/100));
	$total_amount_db =  sprintf("%.2f",($asset_amount + $gst_amount_db - $discount));
	
	if($gst_amount_db!=$gst_amount)
	{
		echo "<script>
			alert('GST amount not verified !');
			$('#loadicon').hide();
			$('#buttonApprove1').attr('disabled', false);
		</script>";
		exit();
	}
}
else
{
	$total_amount_db = $total_amount;
}

if($total_amount_db!=$total_amount)
	{
		echo "<script>
			alert('Total amount not verified !');
			$('#loadicon').hide();
			$('#buttonApprove1').attr('disabled', false);
		</script>";
		exit();
	}
	
if($payment_mode=='NEFT' AND ($acname=='' || $acno==''))
{
	echo "<script>
		alert('Invalid account details !');
		$('#loadicon').hide();
		$('#buttonApprove1').attr('disabled', false);
	</script>";
	exit();
}

if($rowParty['legal_name']!=$party)
{
	echo "<script>
		alert('Party verification failed.');
		$('#loadicon').hide();
		$('#buttonApprove1').attr('disabled', false);
	</script>";
	exit();
}

$VerifyRequest = Qry($conn,"SELECT req_code,ho_approval,branch FROM asset_request WHERE id='$req_id'");
if(!$VerifyRequest){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($VerifyRequest)==0)
{
	echo "<script>
		alert('Asset request not found.');
		$('#loadicon').hide();
		$('#buttonApprove1').attr('disabled', false);
	</script>";
	exit();
}

$rowAsset = fetchArray($VerifyRequest);

if($rowAsset['ho_approval']!=0)
{
	echo "<script>
		alert('Asset Request already approved.');
		$('#loadicon').hide();
		$('#buttonApprove1').attr('disabled', true);
	</script>";
	exit();
}

// echo "<script>
		// alert('Function is in-active.');
		// $('#loadicon').hide();
		// $('#buttonApprove1').attr('disabled', false);
	// </script>";
	// exit();
	
$asset_branch = $rowAsset['branch'];

if($payment_mode=='CASH' AND $total_amount>10000)
{
	echo "<script>
		alert('Please check asset amount max cash allowed is 10000');
		$('#buttonApprove1').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}

	$vou_no_Qry = GetVouId("E",$conn,"mk_venf","vno",$asset_branch);
	
	if(!$vou_no_Qry || $vou_no_Qry=="0" || $vou_no_Qry==''){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		Redirect("Error while processing Request","./");
		exit();
	}
		
	$expid=$vou_no_Qry;
	
$chk_vou_no = Qry($conn,"SELECT id FROM mk_venf WHERE vno='$expid'");

if(!$chk_vou_no){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
	Redirect("Error while processing Request","./");
	exit();
}
		
if(numRows($chk_vou_no)>0)
{
	echo "<script>
		alert('Duplicate Voucher No: $expid. Try again !');
		$('#buttonApprove1').attr('disabled',false);
		$('#loadicon').hide();
	</script>";
	exit();
}	


	$check_balance = Qry($conn,"SELECT balance,balance2,email FROM user WHERE username='$asset_branch'");
	if(!$check_balance){
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
		Redirect("Error while processing Request","./");
		exit();
	}
	
	if(numRows($check_balance)==0)
	{
		echo "<script>
			alert('Branch $asset_branch not found !');
			$('#buttonApprove1').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
	
	$row_balance = fetchArray($check_balance);
	$rrpl_balance = $row_balance['balance'];
	$rr_balance = $row_balance['balance2'];
	$email = $row_balance['email'];
	
if($payment_mode=='CASH')
{	
	if($company=='RRPL' && $rrpl_balance>=$total_amount)
	{
		$new_amount = $rrpl_balance-$total_amount;
		$balance_col="balance";
		$debit_col="debit";
	}
	else if($company=='RAMAN_ROADWAYS' && $rr_balance>=$total_amount)
	{
		$new_amount = $rr_balance-$total_amount;
		$balance_col="balance2";
		$debit_col="debit2";
	}
	else
	{
		echo "<script>
			alert('Insufficient balance in company : $company($asset_branch).');
			$('#buttonApprove1').attr('disabled',false);
			$('#loadicon').hide();
		</script>";
		exit();
	}
}

	if($company=='RRPL')
	{
		$balance_col="balance";
		$debit_col="debit";
	}
	else if($company=='RAMAN_ROADWAYS')
	{
		$balance_col="balance2";
		$debit_col="debit2";
	}
	else
	{
		echo "<script>
			alert('Invalid company : $company.');
			window.location.href='./vehicle_approval.php';
		</script>";
		exit();
	}	
	
StartCommit($conn);
$flag = true;

if($payment_mode=='CASH')
{
	$update_balance = Qry($conn,"update user set `$balance_col`='$new_amount' where username='$asset_branch'");
	if(!$update_balance){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$insert_cash = Qry($conn,"INSERT INTO cashbook(user,date,vou_date,comp,vou_no,vou_type,desct,`$debit_col`,`$balance_col`,timestamp) 
	VALUES ('$asset_branch','$date','$date','$company','$expid','Expense_Voucher','NEW_ASSET','$total_amount','$new_amount','$timestamp')");

	if(!$insert_cash){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($payment_mode=='CHEQUE')
{
	$insert_passbook = Qry($conn,"INSERT INTO passbook(user,vou_no,date,vou_date,comp,vou_type,desct,chq_no,`$debit_col`,timestamp) VALUES 
	('$asset_branch','$expid','$date','$date','$company','Expense_Voucher','NEW_ASSET','$cheque_no','$total_amount','$timestamp')");
	
	if(!$insert_passbook){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
$insert_cheque_book=Qry($conn,"INSERT INTO cheque_book(vou_no,vou_type,vou_date,amount,cheq_no,date,branch,timestamp) VALUES 
('$expid','Expense_Voucher','$date','$total_amount','$cheque_no','$date','$asset_branch','$timestamp')");

	if(!$insert_cheque_book){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else if($payment_mode=='NEFT')
{
	$chk_neft = Qry($conn,"SELECT id FROM rtgs_fm WHERE fno='$expid'");
	if(!$chk_neft){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	if($company=='RRPL')
	{
		$get_Crn = GetCRN("RRPL-E",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		$crnnew = $get_Crn;
	}
	else
	{
		$get_Crn = GetCRN("RR-E",$conn);
		if(!$get_Crn || $get_Crn=="0" || $get_Crn=="")
		{
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
			$flag = false;
		}
		$crnnew = $get_Crn;
	}
	
	if(numRows($chk_neft)==0)		
	{
		$qry_rtgs=Qry($conn,"INSERT INTO rtgs_fm(fno,branch,com,totalamt,amount,acname,acno,bank_name,ifsc,pan,pay_date,fm_date,type,type2,
		email,crn,timestamp) VALUES ('$expid','$asset_branch','$company','$total_amount','$total_amount','$acname','$acno','$bank_name',
		'$ifsc','$pan','$date','$date','EXPENSE_VOU','NEW_ASSET','$email','$crnnew','$timestamp')");			

		if(!$qry_rtgs){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$flag = false;
		errorLog("Duplicate Rtgs Voucher : $expid",$conn,$page_name,__LINE__);
	}
}
else
{
	$flag = false;
	errorLog("Invalid payment mode selected.",$conn,$page_name,__LINE__);
}	
	
$insert_voucher = Qry($conn,"INSERT INTO mk_venf(user,branch_user,vno,newdate,date,comp,des,desid,asset_voucher,amt,amt_w,chq,chq_no,chq_bnk_n,neft_bank,
neft_acname,neft_acno,neft_ifsc,pan,narrat,empcode,vehno,cash_sign,rcvr_sign,timestamp,upload) VALUES ('$asset_branch',
'CGROAD','$expid','$date','$date','$company','NEW_ASSET','','1','$total_amount','','$payment_mode','$cheque_no','','$bank_name','$acname','$acno',
'$ifsc','$pan','NEW Asset Purchase : Maker: $maker, Modal: $model.','','','','','$timestamp','')");

if(!$insert_voucher){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}

$TodayData = Qry($conn,"UPDATE today_data SET exp_vou=exp_vou+1,exp_vou_amount=exp_vou_amount+'$total_amount' WHERE branch='$asset_branch'");

if(!$TodayData){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}
	
$save_approval = Qry($conn,"UPDATE asset_request SET party_id='$party_id',maker='$maker',model='$model',
payment_mode='$payment_mode',vou_no='$expid',amount='$asset_amount',gst_invoice='$is_gst_invoice',gst_type='$gst_type',
gst_value='$gst_value',gst_amount='$gst_amount',discount='$discount',payment_amount='$total_amount',ho_approval='1',ho_approval_time='$timestamp' 
WHERE id='$req_id'");

if(!$save_approval){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);	
}

$insert_log = Qry($conn,"INSERT INTO edit_log(vou_no,vou_type,section,edit_desc,branch,timestamp) VALUES ('$rowAsset[req_code]','Asset',
'Approve','Asset Request Approved.','ADMIN','$timestamp')");

if(!$insert_log){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		alert('Asset Request : $rowAsset[req_code]. Approved Successfully !');
		$('#mainModalHide2').click();
		$('#ApproveAsset$req_id').html('Approved');
		$('#DeleteReq$req_id').attr('disabled', true);
		$('#ApproveAsset$req_id').attr('disabled', true);
		$('#loadicon').hide();
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	Redirect("Error While Processing Request.","./vehicle_approval.php");
	exit();
}
?>