<?php 
require_once './_connect.php';
$date = date("Y-m-d"); 

include ("./_header.php");
?>
<div class="container-fluid" style="">
	<div class="row">
		<div class="form-group col-md-12" style="font-size:17px;">
			Logged in as : <font color="green"><?php echo "Asset Manager"; ?></font>
			<br />
			<br />
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<h4 class="" style="color:#000;padding:6px;font-size:15px;">Assets & Employees </h4>
		</div>
		<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
				<div class="panel panel-blue panel-widget" style="border:0px solid #000;background:#F5F5F5">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="../../b5aY6EZzK52NA8F/svg/employee.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5;color:000">
						<span id="TotalEmpSpan"></span>
							<div class="text-muted" style="color:#000; font-size:15px; padding-top:6px;">
							<a style="color:maroon" href="./_report.php?report_name=Total Employees">Total Employees</a>
							</div>
						</div>
					</div>
				</div>
			</div>	
                        		

			<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
				<div class="panel panel-orange panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="../../b5aY6EZzK52NA8F/svg/vehicle.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5;color:000">
						<span id="TotalVehSpan"></span>
							<div class="text-muted" style="color:#000; font-size:15px;padding-top:6px;">
							<a style="color:maroon" href="./_report.php?report_name=Total Vehicles">Total Vehicles</a>
							</div>
						</div>
					</div>
				</div>
			</div>	
                        		

			<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
				<div class="panel panel-teal panel-widget" style="border:0px solid #000">
					<div class="row no-padding">
						<div class="col-md-3 widget-left">
							<img class="img-responsive" style="width:100%;height:100%" src="../../b5aY6EZzK52NA8F/svg/assets.svg">
						</div>
						<div class="col-md-9 widget-right" style="background:#F5F5F5;color:000">
						<span id="TotalAssetSpan"></span>
							<div class="text-muted" style="color:#000; font-size:15px;padding-top:6px;">
							<a style="color:maroon" href="./_report.php?report_name=Total Assets">Total Assets</a>
							</div>
						</div>
					</div>
				</div>
			</div>	
                        		
	</div>
	
</div>

</body>

<div id="response"></div>

<script>
$(document).ready(function(){
$('#loadicon').show();
$.ajax({
   url: 'load_home.php',
   type: 'post',
   data: {name: name},
   success: function(response){
    $('#response').html(response);
   }
  });
});
</script>